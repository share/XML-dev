xquery version "3.1";

import module namespace config = "http://www.hisoma.mom.fr/edinum/epick/config" at "config.xqm";
import module namespace transform = "http://exist-db.org/xquery/transform" at "java:org.exist.xquery.functions.transform.TransformModule";

let $docId := request:get-parameter('docId', ())
let $xml := doc($config:app-root || "/data/" || "/xml/" || $docId || ".xml")
(:let $xml-test := doc("/db/apps/cachetteexistdb/data/xml/CK22.xml"):)
let $xslt := doc($config:app-root || "/resources/xslt/Stylesheets-cachette" || "/start-edition.xsl")


let $xslParamMap :=
map {
	"method": "xml",
	"indent": false(),
	"item-separator": ""
	
}
let $xslParam :=
<output:serialization-parameters
	xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
	<output:omit-xml-declaration
		value="yes"/>
	<output:method
		value="xml"/>
	<output:indent
		value="yes"/>
	<output:item-separator
		value="\n"/>
	<output:undeclare-prefixes
		value="yes"/>
</output:serialization-parameters>



return
	(:$xml:)
	transform:transform($xml, $xslt, $xslParam)

