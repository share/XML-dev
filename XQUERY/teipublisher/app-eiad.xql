xquery version "3.1";

(: Module for app-specific template functions :)
module namespace app="teipublisher.com/app";

import module namespace templates="http://exist-db.org/xquery/templates";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace pages="http://www.tei-c.org/tei-simple/pages" at "lib/pages.xql";
import module namespace search="http://www.tei-c.org/tei-simple/search" at "search.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace tpu="http://www.tei-c.org/tei-publisher/util" at "../lib/util.xql";
import module namespace kwic="http://exist-db.org/xquery/kwic" at "resource:org/exist/xquery/lib/kwic.xql";
import module namespace nav="http://www.tei-c.org/tei-simple/navigation" at "navigation.xql";


declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace functx = "http://www.functx.com";


(:declare variable $app:data-images-root := $config:data-images-root || "PYU001";:)


declare
    %templates:wrap
function app:foo($node as node(), $model as map(*)) {
	let $folder := $model("work")/ancestor-or-self::tei:TEI//tei:idno[@type='filename']
	let $images-path := $config:data-images-root || $folder || '/'
		return
    		<p>
   			Dummy templating function. {map:size($model)} 
   			<br/>folder : {$folder}
   			<br/>images-path : {$images-path}
   			<br/>{ for $i in collection($images-path) return util:document-name($i)}
   			</p>
   			
};


declare
    %templates:wrap
function app:foo2($node as node(), $model as map(*)) {
	<div>
	   <p>{map:keys($model)}</p>
	   <p>{$model?config?*}</p>
	   <p>{map:keys($model?config)}</p>
	    <p>{$model?config?next}</p>
	    <p>{map:keys($model?configuration)}</p>
	    <p>{$model?configuration?app-root}</p>
	</div>
};


(:  ---------------------------------------------------------------------------- :)
(:                  #EM  - epigraphic metadata from teiHeader               :)
(:  ---------------------------------------------------------------------------- :)
(:  built from app:short-header in lib/browse.xql :)
declare
    %templates:wrap
    %templates:default("headerType", "epidoc")
function app:metadata($node as node(), $model as map(*), $headerType as xs:string) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $relPath := config:get-identifier($work)   
    return
        $pm-config:web-transform($work/tei:teiHeader, map {
            "headerType": 'epidoc',
            "doc": $relPath || "?odd=" || $model?config?odd
        }, $model?config?odd)
};

(:  built from app:short-header in lib/browse.xql :)
declare
    %templates:wrap
function app:short-header($node as node(), $model as map(*)) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $relPath := config:get-identifier($work)
    return
        $pm-config:web-transform($work/tei:teiHeader, map {
            "header": "short",
            "doc": $relPath || "?odd=" || $model?config?odd
        }, $model?config?odd)
};


(:  built from pages:navigation-title in lib/browse.xql :)
(: to reorganize more logic in function names :)
declare
    %templates:wrap
function app:navigation-title($node as node(), $model as map(*)) {
    app:title($model('data')/ancestor-or-self::tei:TEI)
};

declare
    %templates:wrap
function app:banner-title($node as node(), $model as map(*)) {
    app:title-simple($model('data')/ancestor-or-self::tei:TEI)
};

declare
    %templates:wrap
function app:banner-idno($node as node(), $model as map(*)) {
    app:idno($model('data')/ancestor-or-self::tei:TEI)
};

(:  built from pages:title in lib/browse.xql :)
(: adds the idno inventory number :)
(:declare function app:title($work as element()) {
	let $idno := 
		if (exists($work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno)) then 
			$work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno  || ' - '
		else ()
    let $main-title := 
    	$work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main']/text() 
    return	
        if ($main-title) then $idno || $main-title 
        else 
        	$idno || $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]/text()
};:)

declare function app:title($work as element()) {
	let $idno := 
		if (exists($work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno)) then 
			$work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno  || ' - '
		else ()
    let $main-title := 
    	$work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main']
    return	
        if ($main-title) then $idno || $main-title 
        else 
        	$idno || $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]
};



(: without inventory number :)
declare function app:title-simple($work as element()) {
    let $main-title := 
    	$work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main'] 
    return	
        if ($main-title) then $main-title 
        else 
        	$work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]
};

(: just the inventory number :)
declare function app:idno($work as element()) {
    let $idno := 
		if (exists($work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno)) then 
			$work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno
		else $work/tei:teiHeader/tei:fileDesc//tei:idno[@type='filename']
    return	
        $idno
};


declare
    %templates:wrap
function app:title-keywords($node as node(), $model as map(*)) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $keywords := $work/tei:teiHeader//tei:sourceDesc//(tei:objectType | tei:material | tei:placeName | tei:origDate)
    let $identifier := $work/tei:teiHeader//tei:sourceDesc//tei:altIdentifier[@type='museum-inventory']
    return
        <div class="text-center">
            <h4>{$identifier/tei:repository} - {$identifier/tei:idno}</h4>            
            <div id="keywords">
                { for $i in $keywords   
                    let $title := if (fn:name($i) = 'origDate') then
                                    concat($i/@notBefore, '-', $i/@notAfter) else $i
                    return    
                        <a href="#" class="btn btn-raised btn-default btn-xs">{$title}</a>
                 }
            </div>
        </div>
};
(:
(\:~
 : from browse.xql app:work-title - to display the idno in the search results
 :\)
declare function app:work-title-custom($node as node(), $model as map(*), $type as xs:string?) {
    let $suffix := if ($type) then "." || $type else ()
    let $work := $model("work")/ancestor-or-self::tei:TEI
    (\: modif #EM :\)
    let $idno := 
        if (exists($work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno)) then 
			$work/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msIdentifier/tei:idno
		else $work/tei:teiHeader/tei:fileDesc//tei:idno[@type='filename']/text()
    (\: end modif #EM :\)
    let $id := util:document-name($work)
    return
        <a href="{$node/@href}{$id}{$suffix}">{$idno} – { app:work-title-custom($work) } </a>
};


declare function app:work-title-custom($work as element(tei:TEI)?) {
    
    let $main-title := $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type = 'main']/text()
    let $main-title := if ($main-title) then $main-title else $work/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1]/text()
        return
         $main-title
};:)

(:  ---------------------------------------------------------------------------- :)
(:                  #EM  - main content               :)
(:  ---------------------------------------------------------------------------- :)

(:~
 : List documents in data collection
 :)
declare
    %templates:wrap
function app:list-works($node as node(), $model as map(*), $filter as xs:string?, $root as xs:string,
    $browse as xs:string?) {
    let $cached := session:get-attribute("simple.works")
    let $filtered :=
        if ($filter) then
            let $ordered :=
                for $rootCol in $config:data-root
                for $item in
                    ft:search($rootCol || "/" || $root, $browse || ":" || $filter, ("author", "title"))/search
                let $author := $item/field[@name = "author"]
                order by $author[1], $author[2], $author[3]
                return
                    $item
            for $doc in $ordered
            return
                doc($doc/@uri)/tei:TEI
        else if ($cached and $filter != "") then
            $cached
        else
            $config:data-root ! collection(. || "/" || $root)/tei:TEI
    return (
        session:set-attribute("simple.works", $filtered),
        session:set-attribute("browse", $browse),
        session:set-attribute("filter", $filter),
        map {
            "all" : $filtered,
            "mode": "browse"
        }
    )
};



declare
    %templates:default("action", "browse")
function app:view($node as node(), $model as map(*), $action as xs:string) {
    let $view := pages:determine-view($model?config?view, $model?data)   
    let $data :=
        if ($action = "search" and exists(session:get-attribute("apps.simple.query"))) then
            let $query := session:get-attribute("apps.simple.query")
            let $div :=
                if ($model?data instance of element(tei:pb)) then
                    let $nextPage := $model?data/following::tei:pb[1]
                    return
                        if ($nextPage) then
                            ($model?data/ancestor::* intersect $nextPage/ancestor::*)[last()]
                        else
                            ($model?data/ancestor::tei:div, $model?data/ancestor::tei:body)[1]
                else
                    $model?data
            let $expanded :=
                util:expand(
                    (
                        search:query-default-view($div, $query),
                        $div[.//tei:head[ft:query(., $query)]]
                    ), "add-exist-id=all"
                )
            return
                if ($model?data instance of element(tei:pb)) then
                    $expanded//tei:pb[@exist:id = util:node-id($model?data)]
                else
                    $expanded
        else
     		$model?data  
       
    let $xml :=
        if ($view = ("div", "page", "body")) then
            pages:get-content($model?config, $data[1])
        else
            $model?data//*:body/* 
   
    return   
            app:process-content($xml, $model?data, $model?config?odd)
};





(: built from lib/pages.xsl :)
(: For transcriptions - adding: tabs :)
declare
    %templates:default("action", "browse")
function app:view-with-tabs($node as node(), $model as map(*), $action as xs:string) {
    let $view := pages:determine-view($model?config?view, $model?data)   
    let $data :=
        if ($action = "search" and exists(session:get-attribute("apps.simple.query"))) then
            let $query := session:get-attribute("apps.simple.query")
            let $div :=
                if ($model?data instance of element(tei:pb)) then
                    let $nextPage := $model?data/following::tei:pb[1]
                    return
                        if ($nextPage) then
                            ($model?data/ancestor::* intersect $nextPage/ancestor::*)[last()]
                        else
                            ($model?data/ancestor::tei:div, $model?data/ancestor::tei:body)[1]
                else
                    $model?data
            let $expanded :=
                util:expand(
                    (
                        search:query-default-view($div, $query),
                        $div[.//tei:head[ft:query(., $query)]]
                    ), "add-exist-id=all"
                )
            return
                if ($model?data instance of element(tei:pb)) then
                    $expanded//tei:pb[@exist:id = util:node-id($model?data)]
                else
                    $expanded
        else
     		$model?data  
       
    let $xml :=
        if ($view = ("div", "page", "body")) then
            pages:get-content($model?config, $data[1])
        else
            $model?data//*:body/* 
    let $xml-edition := $xml//(tei:div[@type='edition'] | tei:div[@type='apparatus'])
    let $xml-other-divs := $xml except $xml-edition
    return   
    
        ( 
           (: app:process-tabs($xml-edition, $model?data, $model?config?odd),:)
            app:process-content($xml-other-divs, $model?data, $model?config?odd)
         )
};


(: inserts edition + apparatus wit navigation tabs (logical, physical, xml) :)
declare function app:process-tabs($xml-edition as element()*, $root as element()*, $odd as xs:string) {
    let $tabs := array {"Logical","Physical","XML"}
    let $status := if ($tabs = 'Logical') then 'active' else ()
    return    
    
       	<div class="{$config:css-content-class}">       	
               
             <div class="card card-nav-tabs card-plain"> 
             
                 <div class="header header-success">
                     <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                     <div class="nav-tabs-navigation">
                         <div class="nav-tabs-wrapper">
                            
                             <ul class="nav nav-tabs" role="tablist" data-tabs="tabs">
                                 <li role="presentation" class="{$status}"><a href="#{$tabs?1}" aria-controls="..." role="tab" data-toggle="tab">{$tabs?1}</a></li>
                                 <li role="presentation"><a href="#{$tabs?2}" aria-controls="profile" role="tab" data-toggle="tab">{$tabs?2}</a></li>
                                 <li role="presentation"><a href="#{$tabs?3}" aria-controls="profile" role="tab" data-toggle="tab">{$tabs?3}</a></li>
                             </ul>
                             <a class="accordion-toggle" data-toggle="collapse" href="#collapse-edition">
                              
                             </a>
                 		</div>
                 	</div>
                 </div> 
                 
                
                 
             </div>
             
              <div class="card-content">
                    <div class="tab-content">
                        {   
                            for $i in 1 to array:size($tabs) 
                            let $tabId := $tabs($i)
                            return
                                app:process-content-tabs($xml-edition,$root,$tabId,$odd)
                        }
                    </div>
                </div>
                    
       </div>
};


(: copied from lib/pages.xsl :)
declare function app:process-content($xml as element()*, $root as element()*, $odd as xs:string) {
    
	let $html := $pm-config:web-transform($xml, map { "root": $root }, $odd)
    let $class := if ($html//*[@class = ('margin-note')]) then "margin-right" else ()
    let $body := pages:clean-footnotes($html)
    let $test := $root
    return
        <div class="{$config:css-content-class} {$class}">
        {
            $body,
            if ($html//li[@class="footnote"]) then
                <div class="footnotes">
                    <ol>
                    {
                        for $note in $html//li[@class="footnote"]
                        order by number($note/@value)
                        return
                            $note
                    }
                    </ol>
                </div>
            else
                ()
        }
        </div>
};

declare function app:process-content-tabs($xml-edition as element()*, $root as element()*, $tabId as xs:string, $odd as xs:string) {
	let $html := $pm-config:web-transform($xml-edition, map { "root": $root, "break":$tabId }, $odd)
    let $class := if ($html//*[@class = ('margin-note')]) then "margin-right" else ()
    let $body := pages:clean-footnotes($html)
    let $status := if ($tabId = 'Logical') then ' active' else ()
    return
        <div role="tabpanel" class="tab-pane{$status}" id="{$tabId}">
        {
            $body,
            if ($html//li[@class="footnote"]) then
                <div class="footnotes">
                    <ol>
                    {
                        for $note in $html//li[@class="footnote"]
                        order by number($note/@value)
                        return
                            $note
                    }
                    </ol>
                </div>
            else
                ()
        }       
        </div>        
};


(:  ------------------------------------------------------------------------------------------------------------------------ :)
(:                  #EM ajouts - templates/doc-table: lien avec param pour les fichiers sans tabs (bibliography, index...    :)
(:  ------------------------------------------------------------------------------------------------------------------------ :)
(: from app:short-header in lib/browse.xql :)
declare
    %templates:wrap
function app:short-header($node as node(), $model as map(*)) {
    let $work := $model("work")/ancestor-or-self::tei:TEI
    let $relPath := config:get-identifier($work)
     (: test for parameter tabs: to change with global parameter from config :)
    let $tabs := if (exists($work/@xml:id)) then 'tabs=no' else ()
    
    return
        $pm-config:web-transform($work/tei:teiHeader, map {
            "header": "short",
            "doc": $relPath || '?' ||  $tabs || "&amp;odd=" || $model?config?odd
        }, $model?config?odd)
};

(:  ---------------------------------------------------------------------------- :)
(:                  #EM corpus toc                                               :)
(:  ---------------------------------------------------------------------------- :)
(: #EM modifie juste le préfixe dans l'appel de fonction ('app' à la place de 'pages') (si appel direct de la fonction dans le module 'pages', alors les liens ne fonctionnent plus :)
declare function app:load-xml($view as xs:string?, $root as xs:string?, $doc as xs:string) {
    let $data := pages:get-document($doc)
    let $config := tpu:parse-pi(root($data), $view)
    return
        map {
            "config": $config,
            "data":
                switch ($config?view)
            	    case "div" return
                        if ($root) then
                            let $node := util:node-by-id($data, $root)
                            return
                                 $node/ancestor-or-self::tei:div[count(ancestor::tei:div) < $config:pagination-depth][1]
                        else
                            let $div := ($data//tei:div)[1]
                            return
                                if ($div) then
                                    $div
                                else
                                    let $group := $data/tei:TEI/tei:text/tei:group/tei:text/(tei:front|tei:body|tei:back)
                                    return
                                        if ($group) then
                                            $group[1]
                                        else
                                            $data/tei:TEI//tei:body
                    case "page" return
                        if ($root) then
                            util:node-by-id($data, $root)
                        else
                            let $div := ($data//tei:pb)[1]
                            return
                                if ($div) then
                                    $div
                                else
                                    $data/tei:TEI//tei:body
                    default return
                        if ($root) then
                            util:node-by-id($data, $root)
                        else
                            $data/tei:TEI/tei:text
        }
};

(: #EM ajout : $corpus :)
declare
    %templates:wrap
function app:load($node as node(), $model as map(*), $doc as xs:string, $root as xs:string?,
    $id as xs:string?, $view as xs:string?, $corpus as xs:string?) {
    let $doc := 
    	if ($corpus) then
    		xmldb:decode($corpus)
    	else
    		xmldb:decode($doc)
    let $data :=
    	if ($corpus) then
    		let $id := 'project'
            let $node := doc($config:data-root || "/" || $doc)/id($id)
            let $div := $node/ancestor-or-self::tei:div[1]
            let $config := tpu:parse-pi(root($node), $view)
            return
                map {
                    "config": $config,
                    "data":
                        if (empty($div)) then
                        	$node
                        else
                            $node
                }
        else if ($id) then
            let $node := doc($config:data-root || "/" || $doc)/id($id)
            let $div := $node/ancestor-or-self::tei:div[1]
            let $config := tpu:parse-pi(root($node), $view)
            return
                map {
                    "config": $config,
                    "data":
                        if (empty($div)) then
                            $node/following-sibling::tei:div[1]
                        else
                            $div
                }
        else
            app:load-xml($view, $root, $doc)
    let $node :=
        if ($data?data) then
            $data?data
        else
            <TEI xmlns="http://www.tei-c.org/ns/1.0">
                <teiHeader>
                    <fileDesc>
                        <titleStmt>
                            <title>Not found</title>
                        </titleStmt>
                    </fileDesc>
                </teiHeader>
                <text>
                    <body>
                        <div>
                            <head>Failed to load!!</head>
                            <p>Could not load document {$doc}. Maybe it is not valid TEI or not in the TEI namespace?</p>
                        </div>
                    </body>
                </text>
            </TEI>//tei:div
    return
        map {
            "config": $data?config,
            "data": $node
        }
};
(: static pages :)
declare
    %templates:wrap
function app:load-static($node as node(), $model as map(*), $doc as xs:string, $root as xs:string?,
    $id as xs:string?, $view as xs:string?, $corpus as xs:string?) {
    let $doc := xmldb:decode($doc)
    let $data := app:load-xml($view, $root, $doc)
    let $node :=
        if ($data?data) then
            $data?data
        else
            <TEI xmlns="http://www.tei-c.org/ns/1.0">
                <teiHeader>
                    <fileDesc>
                        <titleStmt>
                            <title>Not found</title>
                        </titleStmt>
                    </fileDesc>
                </teiHeader>
                <text>
                    <body>
                        <div>
                            <head>Failed to load!!</head>
                            <p>Could not load document {$doc}. Maybe it is not valid TEI or not in the TEI namespace?</p>
                        </div>
                    </body>
                </text>
            </TEI>//tei:div
    return
        map {
            "config": $data?config,
            "data": $node
        }
};

(: #EM modifie juste le préfixe dans l'appel de fonction ('app' à la place de 'pages') :)
declare
    %templates:wrap
function app:table-of-contents($node as node(), $model as map(*)) {
    console:log($model?data/preceding::tei:div[last()]/tei:head),
    let $tabs := 
    	if (exists($model?data/ancestor-or-self::tei:TEI/@xml:id)) then 'tabs=no' else ()
    let $current :=
        if ($model?config?view = "page") then
            ($model?data/ancestor-or-self::tei:div[1], $model?data/following::tei:div[1])[1]
        else
            $model?data
    return
        app:toc-div(root($model?data), $model?config?view, $current, $model?config?odd, $tabs)
};



(: #EM modifié liens dans toc pour avoir liens directs :)  
 declare %private function app:toc-div($node, $view as xs:string?, $current as element(), $odd as xs:string, $tabs) {
    let $view := pages:determine-view($view, $node)
	let $divs := $node//tei:div[tei:head] except $node//tei:div[tei:head]//tei:div	
    return
        <ul>
        {
            for $div in $divs
            let $html :=
                if ($div/tei:head/*) then
                    $pm-config:web-transform($div/tei:head, map { "header": "short", "root": $div }, $odd)
                else
                    $div/tei:head/text()
            let $root := (
                if ($view = "page") then
                    ($div/*[1][self::tei:pb], $div/preceding::tei:pb[1])[1]
                else
                    (),
                $div
            )[1]
            let $id := "T" ||util:uuid()
            let $hasDivs := exists($div//tei:div[tei:head] except $div//tei:div[tei:head]//tei:div)
            let $isIn := if ($div/descendant::tei:div[. is $current]) then "in" else ()
            let $isCurrent := if ($div is $current) then "active" else ()
            let $icon := if ($isIn) then "expand_less" else "expand_more"
           	let $redirect := 
           		if ($div/tei:head[1]/@n) then $div/tei:head[1]/@n 
           		else
           			if ($div/@type = 'redirect-links') then
           				'home'
           			else ()
            let $href :=
            	if (exists($redirect)) then
            		if ($redirect = 'home') then 
            			(request:get-context-path() || substring-after($config:app-root, "/db")) || '/works/'
            		else 
            			$redirect
            	else 
            		(util:document-name($div) || '?' || $tabs || '&amp;root=' || util:node-id($root) || '&amp;odd=' || $odd  || '&amp;view=' || $view)
            let $data-doc :=
            	if (exists($redirect)) then
            			$redirect
            	else 
            		(config:get-identifier($div))
            let $data-div :=
            	if (exists($redirect)) then
            		()
            	else 
            		(util:node-id($div))
                      		
            return
                <li>
                    {
                        if ($hasDivs) then
                            <a data-toggle="collapse" href="#{$id}"><span class="material-icons">{$icon}</span></a>
                        else
                            ()
                    }
                    <a data-doc="{$data-doc}" data-div="{$data-div}" class="toc-link {$isCurrent}"
                        href="{$href}">{$html}</a>
                    {
                        if ($hasDivs) then
                            <div id="{$id}" class="collapse {$isIn}">{app:toc-div($div, $view, $current, $odd, $tabs)}</div>
                        else
                            app:toc-div($div, $view, $current, $odd, $tabs)
                    }
                </li>
        }
        </ul>
};


(:  --------------------------------------------------- :)
(:                 image galleries                      :)
(:  --------------------------------------------------- :)


(: à tester :)
declare function app:get-image($idOrName as xs:string) {
    if ($config:address-by-id) then
        root(collection($config:data-root)/id($idOrName))
    else
        doc($config:data-root || "/" || $idOrName)
};

declare
    %templates:wrap
function app:banner($node as node(), $model as map(*), $imagesContainer as xs:string?) {
    let $images := 
        if ($imagesContainer = 'facsimile' and exists($model("work")/ancestor-or-self::tei:TEI/tei:facsimile/tei:graphic[@xml:id])) then 'yes'
            else ()
    let $folder := $model("work")/ancestor-or-self::tei:TEI//tei:idno[@type='filename']
	let $images-path := $config:data-images-root || $folder
		return ( 
		      session:set-attribute("images", $images),
        	   map { 
        		    "images" := $images,
        		    "imagesContainer" := $imagesContainer,
        			"images-path" := $images-path
        		}
        )
          
};


declare
    %templates:wrap 
function app:carousel($node as node(), $model as map(*)) {   
    let $images-path := $model?images-path
    let $images-info := $model("work")/ancestor-or-self::tei:TEI/tei:facsimile/tei:graphic[@xml:id]
	let $coll-images := 
	   if ($model?imagesContainer ='folderContent') then	       
	       collection($model?images-path)
	   else
	       $images-info/@url
		return
           <div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="">         
                <!-- Carousel indicators -->
                <ol class="carousel-indicators"> 
                    { for $i at $pos in $coll-images
                        let $status :=                            
                                   if ($model?imagesContainer ='facsimile' and exists($i) and $i/parent::tei:graphic[@n='1']) then 
                                   let $active := "active "
                                          return $active
                                      else ()
                         let $post := $pos - 1
                        return
                            <li data-target="#myCarousel" data-slide-to="{$pos}" class="{$status}"></li>
                            
                     }
                 </ol>
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">                
                    { for $i at $pos in $coll-images                     
                  	     let $filename := 
                           	if ($model?imagesContainer ='facsimile' and exists($i)) then 
                           		data($i)
                           	else if ($model?imagesContainer ='folderContent' and exists($i)) then
                           		util:document-name($i)
                           	else ()
                          let $status :=                            
                               if ($model?imagesContainer ='facsimile' and exists($i) and $i/parent::tei:graphic[@n='1']) then 
                               let $active := "active "
                                      return $active
                                  else ()
                           let $title := 
                              if ($model?imagesContainer ='facsimile' and exists($i)) then 
                      		 $i/parent::tei:graphic//tei:item[1]
                      		  else()
                          let $symbol-copyright :=  "&#169;"
                          let $copyright := 
                                  if ($model?imagesContainer ='facsimile' and (exists($i/parent::tei:graphic//tei:item[4]))) then                   		                         		          $symbol-copyright || ' ' || $i/parent::tei:graphic//tei:item[4] 
                      		          else()
  			              	  return
            	                     let $result :=
            		            			if ($filename != '') then
                   		            			<div class="item {$status}">
                                                    <img src="/exist/{substring-after($model?images-path,'/db/')}/{$filename}" alt="First Slide" class="img-responsive"/>
                                                <div class="carousel-caption">
                                                  <h3>{$title}</h3>
                                                  <p>{$copyright}</p>
                                              </div>
                                                </div>
                                            else ()
                               return $result
                           }                           
                    </div>
                    <!-- Carousel controls -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                    
</div>
  
};




declare
    %templates:wrap
function app:image-gallery($node as node(), $model as map(*), $imagesContainer as xs:string?) {
	let $folder := $model("work")/ancestor-or-self::tei:TEI//tei:idno[@type='filename']
	let $images-path := $config:data-images-root || $folder
		return        
        		map { 
        			"imagesContainer" := $imagesContainer,
        			"images-path" := $images-path
        		}
};

declare
    %templates:wrap 
function app:thumbnails-gallery($node as node(), $model as map(*)) {   
    let $images-path := $model?images-path
    let $images-info := $model("work")/ancestor-or-self::tei:TEI/tei:facsimile/tei:graphic[@xml:id]
	let $coll-images := 
	   if ($model?imagesContainer ='folderContent') then	       
	       collection($model?images-path)
	   else
	       $images-info/@url
		return
		
		<div>
            <ul class="nav nav-tabs thumbnails-banner">
                    { 
                        for $i at $pos in $coll-images                   
                        let $filename := 
                           if ($model?imagesContainer ='facsimile' and exists($i)) then 
                               data($i)
                           else if ($model?imagesContainer ='folderContent' and exists($i)) then
                               util:document-name($i)
                           else ()            
                       
                       let $result :=
           	                if ($filename != '') then
           	                <li>                         
           	                     <a href="#" data-toggle="modal" aria-expanded="false" data-target="#tallModal{$pos}">
           	                         <img id="image{$pos}" class="img-responsive img-thumbnail" src="/exist/{substring-after($images-path,'/db/')}/{$filename}" alt=" profile "/>
           	                     </a>
           	                 </li>       	                 
           	             else ()
           	          return 
           	            $result
    	          	}
                 </ul>
                
             { 
                    for $i at $pos in $coll-images
                    let $filename := 
                        if ($model?imagesContainer ='facsimile' and exists($i)) then 
                            data($i)
                        else if ($model?imagesContainer ='folderContent' and exists($i)) then
                            util:document-name($i)
                        else ()
                    let $url := '/exist/' || fn:substring-after($model?images-path,'/db/') ||'/' || $filename
                    let $title := 
                        if ($model?imagesContainer ='facsimile' and exists($i)) then 
                         $i/parent::tei:graphic//tei:item[1]
                          else()
                    let $copyright := 
                        if ($model?imagesContainer ='facsimile' and exists($i)) then 
                         $i/parent::tei:graphic//tei:item[4]
                          else()
                    let $result :=  
                        
                           <div id="tallModal{$pos}" class="modal modal-wide fade">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#215;</button>
                                  <h4 class="modal-title">{$title}</h4>
                                   <p>{$copyright}</p>
                                </div>
                                <div class="modal-body">
                                  <img id="image{$pos}" class="img-responsive" src="{$url}"/>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                 
                                </div>
                              </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          </div>            
                       return $result
                 }
          </div>
         
};


(: 
 : finds the good bibl node 
:)
declare function app:find-source ($source as xs:string, $root as element()){
	let $doc := doc($config:data-root || '/' || $config:bibliography-masterfile)

	let $citedRange := $root//tei:bibl[@xml:id=(substring-after($source, '#'))]/tei:citedRange 
	let $source :=		
		(: if internal id :)
			if (starts-with($source, "#")) then 
				let $source := substring-after($source, '#') 
				(: to find the target in the master bibliography:)
				let $biblStruct-target := substring-after($root//tei:bibl[@xml:id=$source]/tei:ptr/@target,$config:bibliography-prefix-separator)
				
				return
					app:make-bibl-link($doc//tei:biblStruct[@xml:id=$biblStruct-target],$config:bibliography-masterfile)
					
					
			(: if external id :)
			else if (starts-with($source, $config:bibliography-prefix)) then 
					let $source := substring-after($source,$config:bibliography-prefix-separator)
					let $biblStruct := $doc//tei:biblStruct[@xml:id=$source]	
					
					return
						app:make-bibl-link($doc//tei:biblStruct[@xml:id=$source],$config:bibliography-masterfile)
					
			else 
				 element {'span'} { attribute class {'danger'}, ' !! something is missing !!' } 
		return 
			$source || ' ' || $citedRange
				
};


(:  --------------------------------------------------- :)
(:                 bibliography                         :)
(:  --------------------------------------------------- :)

(: 
 : formatage de l'entrée bibliographique dans la page de la biblio complète  
:)

(:  built from app:short-header in lib/browse.xql :)
(:declare
    %templates:wrap
function app:bibliography-entry($node as node(), $model as map(*)) {
    <ul class="tei-listBibl1 list-group">
        <li class="tei-biblStruct1 list-group-item" id="Afshar1981"><span class="tei-biblStruct2"><a href="#" data-toggle="tooltip" data-placement="right" title="" class="tei-biblStruct3" id="Afshar1981" data-original-title="Afshar1981"><span class="tei-title15 vedette">Afshar 1981</span></a></span><span class="tei-biblStruct23"><span class="tei-author2"><span class="tei-author3">Afshar</span>, <span class="tei-author5">Qobad</span>, </span></span><span class="tei-biblStruct24"></span><span class="tei-biblStruct25"></span><span class="tei-biblStruct26"><span class="tei-title17">An ancient Indian Itinerary: The Geographic Appelatives in the Gaṇḍavyūhasūtra</span></span>, <span class="tei-biblStruct33"><span class="tei-imprint1"><span class="tei-pubPlace">Lausanne</span></span>, <span class="tei-imprint4"><span class="tei-date5">1981</span></span></span><span class="tei-biblStruct34">
               
                  <span class="tei-title17">An ancient Indian Itinerary: The Geographic Appelatives in the Gaṇḍavyūhasūtra</span>
                  <span class="tei-title15 vedette">Afshar 1981</span>
                  <span class="tei-author2"><span class="tei-author3">Afshar</span>, <span class="tei-author5">Qobad</span>, </span>
                  Kronasia Diffusion
                  <span class="tei-imprint1"><span class="tei-pubPlace">Lausanne</span></span>, <span class="tei-imprint4"><span class="tei-date5">1981</span></span>
               
               
            </span></li>
};:)

(: 
 : creates a link to the master bibliography  
 : a vérifier
:)
declare function app:make-bibl-link($ref as node()*, $masterfile as xs:string) {
    let $title-short := $ref//tei:title[@type='short'][1]/text()    
    (:the data contains some markup from the zotero db to tag some italics or sup:)
    let $title-deb := if (contains($title-short, '</i>')) then 
    						(substring-before($title-short,'<i>'))
                       else ($title-short)
    let $title-end := substring-after($title-short,'</i>')
    let $title-mid := substring-before(substring-after($title-short, '<i>'),'</i>')
    let $title-long := $ref//tei:title[not(@type='short')][1]
    let $target := $ref/@xml:id
    (: tabs=no: to avoid the tabpanel and image gallery :)
    let $link := $pages:app-root || '/works/' || $masterfile || '?tabs=no' || '&amp;odd=' || $config:odd || '#' || $target
    return 
		<a href="{$link}" title="{$title-long}" class="refbibl">{$title-deb}<span class="hi">{$title-mid}</span>{$title-end}</a>
};


(: ajout 20171115 :)

(: -------------------------------------------------------------------------------------- :)
(:                   functions based on the modules/lib/search.xql module                  :)
(: -------------------------------------------------------------------------------------- :)

(:~
 : From: search:show-hits.xql
 : Outputs the actual search result as a div, using the kwic module to summarize full text matches.
 : Modification: for an app with $config:default-view := "page" and $config:search-default := "tei:body", 
 : desactivates the reference to tei:pb (as there are no pb in an EpiDoc file, and parameter 'root' in links
 : USED BY: app/search.html
:)
declare
    %templates:wrap
    %templates:default("start", 1)
    %templates:default("per-page", 10)
function app:show-hits($node as node()*, $model as map(*), $start as xs:integer, $per-page as xs:integer, $view as xs:string?) {
    console:log("docs: " || count($model?docs)),
    for $hit at $p in subsequence($model("hits"), $start, $per-page)
    let $parent := ($hit/self::tei:body, $hit/ancestor-or-self::tei:div[1])[1]
    let $parent := ($parent, $hit/ancestor-or-self::tei:teiHeader, $hit)[1]
    let $parent-id := config:get-identifier($parent)
    let $parent-id :=
        if ($model?docs) then replace($parent-id, "^.*?([^/]*)$", "$1") else $parent-id
    let $work := $hit/ancestor::tei:TEI
    (:let $work-title := browse:work-title($work):)
    let $work-title := app:title($work)
    let $config := tpu:parse-pi(root($work), $view)
    let $div := app:get-current($config, $parent)
    let $loc :=
        <tr class="reference">
            <td colspan="3">
                <span class="number">{$start + $p - 1}</span>
                <ol class="headings breadcrumb">
                    <li><a href="{$parent-id}">{$work-title}</a></li>
                    {
                        for $parentDiv in $hit/ancestor-or-self::tei:div[tei:head]
                        let $id := util:node-id(
                            if ($config?view = "page") then $parentDiv/preceding::tei:pb[1] else $parentDiv
                        )
                        return
                            (:<li>
                                <a href="{$parent-id}?action=search&amp;root={$id}&amp;view={$config?view}&amp;odd={$config?odd}">{$parentDiv/tei:head/string()}</a>
                            </li>:)
                            <li>
                                <a href="{$parent-id}?action=search&amp;root={$id}&amp;view={$config?view}&amp;odd={$config?odd}">{$parentDiv/tei:head/string()}</a>
                            </li>
                    }
                </ol>
            </td>
        </tr>
    let $expanded := util:expand($hit, "add-exist-id=all")
    let $docId := config:get-identifier($div)
    let $docId :=
        if ($model?docs) then
            replace($docId, "^.*?([^/]*)$", "$1")
        else
            $docId
    return (
        $loc,
        for $match in subsequence($expanded//exist:match, 1, 5)
        let $matchId := $match/../@exist:id
        let $docLink :=
            if ($config?view = "page") then
                let $contextNode := util:node-by-id($div, $matchId)
               (: let $page := $contextNode/preceding::tei:pb[1]:)
                let $page := $contextNode/ancestor-or-self::tei:body
                return
                    util:node-id($page)
            else
                util:node-id($div)
        (:let $config := <config width="60" table="yes" link="{$docId}?root={$docLink}&amp;action=search&amp;view={$config?view}&amp;odd={$config?odd}#{$matchId}"/>:)
        let $config := <config width="60" table="yes" link="{$docId}?amp;action=search&amp;view={$config?view}&amp;odd={$config?odd}#{$matchId}"/>
        
        return
            kwic:get-summary($expanded, $match, $config)
    )
};

declare %private function app:get-current($config as map(*), $div as element()?) {
    if (empty($div)) then
        ()
    else
        if ($div instance of element(tei:teiHeader)) then
        $div
        else
            if (
                empty($div/preceding-sibling::tei:div)  (: first div in section :)
                and count($div/preceding-sibling::*) < 5 (: less than 5 elements before div :)
                and $div/.. instance of element(tei:div) (: parent is a div :)
            ) then
                nav:get-previous-div($config, $div/..)
            else
                $div
};
