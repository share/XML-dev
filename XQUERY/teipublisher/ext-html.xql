xquery version "3.1";

(:~
 : Non-standard extension functions, mainly used for the documentation.
 :)
module namespace pmf = "http://www.tei-c.org/tei-simple/xquery/ext-html";

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace config = "http://www.tei-c.org/tei-simple/config";
declare namespace pages = "http://www.tei-c.org/tei-simple/pages";
declare namespace functx2 = "http://www.hisoma.mom.fr/labs/functx2";
declare namespace pm-config = "http://www.tei-c.org/tei-simple/pm-config";
declare namespace app = "teipublisher.com/app";
declare namespace functx = "http://www.functx.com";

import module namespace http = "http://expath.org/ns/http-client";
(:import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";:)

(: -------------------------------------------------------------------------------------- :)
(:    Functions copied  from /db/apps/tei-publisher-lib/content/html-functions.xql        :)
(: -------------------------------------------------------------------------------------- :)


declare function pmf:apply-children($config as map(*), $node as element(), $content) {
	if ($node/@xml:id) then
		attribute id {$node/@xml:id}
	else
		(),
	$config?apply-children($config, $node, $content)
};

declare function pmf:inline2($config as map(*), $node as node(), $class as xs:string+, $content) {
	<span
		class="test {$class}">
		{
			$config?apply-children($config, $node, $content)
		}
	</span>
};

(: -------------------------------------------------------------------------------------- :)
(:    Functions copied from html-functions.xql correction                                 :)
(: -------------------------------------------------------------------------------------- :)

declare function pmf:list($config as map(*), $node as node(), $class as xs:string+, $content, $type) {
	if ($node/tei:label) then
		<dl
			class="{$class}">
			{pmf:apply-children($config, $node, $content)}
		</dl>
	else
		let $listType := ($type, $node/@type)[1]
		return
			switch ($listType)
				case "ordered"
					return
						<ol
							class="{$class}">{pmf:apply-children($config, $node, $content)}</ol>
				default return
					<ul
						class="{$class}">{pmf:apply-children($config, $node, $content)}</ul>
};

declare function pmf:listItem($config as map(*), $node as node(), $class as xs:string+, $content,
$n) {
	let $label :=
	if ($node/../tei:label) then
		$node/preceding-sibling::*[1][self::tei:label]
	else
		()
	return
		if ($label) then
			(
			(:<dt>{pmf:apply-children($config, $node, $label)}</dt>,:) (: to avoid label repetition, dt passed via odd :)
			<dd>{pmf:apply-children($config, $node, $content)}</dd>
			)
		else
			<li
				class="{$class}">
				{
					if ($n) then
						attribute value {$n}
					else
						()
				}
				{pmf:apply-children($config, $node, $content)}
			</li>
};


(: -------------------------------------------------------------------------------------- :)
(:    Functions copied from http://www.xqueryfunctions.com/xq/                            :)
(: -------------------------------------------------------------------------------------- :)



declare function functx:substring-after-if-contains
($arg as xs:string?,
$delim as xs:string) as xs:string? {
	if (contains($arg, $delim))
	then
		substring-after($arg, $delim)
	else
		$arg
};

declare function functx:substring-before-if-contains
($arg as xs:string?,
$delim as xs:string) as xs:string? {
	if (contains($arg, $delim))
	then
		substring-before($arg, $delim)
	else
		$arg
};

declare function functx:repeat-string
($stringToRepeat as xs:string?,
$count as xs:integer) as xs:string {
	
	string-join((for $i in 1 to $count
	return
		$stringToRepeat),
	' ')
	(: #EM: space important see cheatsheet :)
};

declare function functx:capitalize-first
($arg as xs:string?) as xs:string? {
	concat(upper-case(substring($arg, 1, 1)),
	substring($arg, 2))
};

declare function functx:chars
($arg as xs:string?) as xs:string* {
	
	for $ch in string-to-codepoints($arg)
	return
		codepoints-to-string($ch)
};

(: -------------------------------------------------------------------------------------- :)
(:    New generic functions                                                               :)
(: -------------------------------------------------------------------------------------- :)

(:~
 : creates a definition list
 : the bootstrap class dl-horizontal can be transmitted via cssClass
 :)
declare function pmf:dl($config as map(*), $node as element(), $class as xs:string+, $content) {
	<dl
		class="{$class}">
		{pmf:apply-children($config, $node, $content)}
	</dl>
};

(:~
 : creates a definition list item title
 :)
declare function pmf:dt($config as map(*), $node as element(), $class as xs:string+, $content) {
	<dt>{pmf:apply-children($config, $node, $content)}</dt>
};

(:~
 : creates a definition list item
 :)
declare function pmf:dd($config as map(*), $node as element(), $class as xs:string+, $content) {
	<dd>{pmf:apply-children($config, $node, $content)}</dd>
};



(:~
 : creates a comment
 :)

declare function pmf:comment($config as map(*), $node as node(), $class as xs:string+, $content) {
	$content ! (
	typeswitch (.)
		case text()
			return
				comment {.}
		default
			return
				comment {text {.}}
	)
};


(: -------------------------------------------------------------------------------------- :)
(:    New epidoc functions                                                               :)
(: -------------------------------------------------------------------------------------- :)

(:~
 : creates a break with ids in EpiDoc style
 :)

declare function pmf:break-epidoc_old($config as map(*), $node as node(), $class as xs:string+, $content, $type as xs:string, $label as item()*, $div-loc as item()*, $number as item()*) {
	
	
	switch ($type)
		case "page"
			return
				<span
					class="{$class}">{pmf:apply-children($config, $node, $label)}</span>
		case "line"
			return
				let $pos-first := fn:generate-id(($node/ancestor::tei:ab[1] | $node/ancestor::tei:p[1])/tei:lb[1])
				let $pos-current := fn:generate-id($node)
				let $arrow-l2r := '&#xa0;&#xa0;→'
				let $arrow-r2l := '&#xa0;&#xa0;←'
				(: the preceding lb -- see if ok when combinaison of textpart and no textpart :)
				let $arrow := if ($pos-first = $pos-current) then
					()
				else
					switch ($node/preceding::tei:lb[1]/@rend)
						case "right-to-left"
							return
								$arrow-r2l (:|| 1|| $pos-first || '-cur' || $pos-current:)
						case "left-to-right"
							return
								$arrow-l2r (:|| 1||$pos-first|| '-cur' || $pos-current :)
						default return
							()
			return
				<span
					class="{$class}">
					<!-- print arrows right of line if R2L or explicitly L2R -->
					<!-- arrows after final line handled in elementSpec ab -->
					{$arrow}<br
						id="a{$div-loc}-l{$number}"
						class="{$class}"/>
					{pmf:apply-children($config, $node, $label)}
				</span>
	case "column"
		return
			<span
				class="{$class}">
				<br
					id="a{$div-loc}-col{$number}"
					class="{$class}"/>
				{pmf:apply-children($config, $node, $label)}
			</span>
	default return
		<span
			class="{$class}">
			<br
				id="a{$div-loc}-{$number}"
				class="{$class}"/>
			{pmf:apply-children($config, $node, $label)}
		</span>
};

declare function pmf:break-epidoc($config as map(*), $node as node(), $class as xs:string+, $content, $type as xs:string, $label as item()*, $div-loc as item()*, $number as item()*) {
	
	
	switch ($type)
		case "page"
			return
				<span
					class="{$class}">{pmf:apply-children($config, $node, $label)}</span>
		case "line"
			return
				let $pos-first := fn:generate-id(($node/ancestor::tei:ab[1] | $node/ancestor::tei:p[1])/tei:lb[1])
				let $pos-current := fn:generate-id($node)
				let $arrow-l2r := '&#xa0;&#xa0;→'
				let $arrow-r2l := '&#xa0;&#xa0;←'
				(: the preceding lb -- see if ok when combinaison of textpart and no textpart :)
				(:let $arrow :=  if($pos-first = $pos-current) then () else
							switch($node/preceding::tei:lb[1]/@rend)
							case "right-to-left" return $arrow-r2l (\:|| 1|| $pos-first || '-cur' || $pos-current:\)
							case "left-to-right" return $arrow-l2r (\:|| 1||$pos-first|| '-cur' || $pos-current :\)
							default return ():)
				let $arrow :=
				switch ($node/@rend)
					case "right-to-left"
						return
							$arrow-r2l
					case "left-to-right"
						return
							$arrow-l2r
					default return
						()
			return
				<span
					class="{$class}">
					<!-- print arrows right of line if R2L or explicitly L2R -->
					<!-- arrows after final line handled in elementSpec ab -->
					<br
						id="a{$div-loc}-l{$number}"
						class="{$class}"/>
					{$arrow}
					{pmf:apply-children($config, $node, $label)}
				</span>
	case "column"
		return
			<span
				class="{$class}">
				<br
					id="a{$div-loc}-col{$number}"
					class="{$class}"/>
				{pmf:apply-children($config, $node, $label)}
			</span>
	default return
		<span
			class="{$class}">
			<br
				id="a{$div-loc}-{$number}"
				class="{$class}"/>
			{pmf:apply-children($config, $node, $label)}
		</span>
};


(:~
 : creates an anchor for milestones EpiDoc style
 :)
declare function pmf:anchor-epidoc($config as map(*), $node as node(), $class as xs:string+, $content, $unit-code as item()*, $div-loc as item()*, $number as item()*) {
	<a
		id="a{$div-loc}-{$unit-code}{$number}"/>
};


(:~
 : creates an ab block with arrow in last line
 :)
declare function pmf:block-with-end($config as map(*), $node as node(), $class as xs:string+, $content, $end as item()*) {
	<div
		class="{$class}">
		{pmf:apply-children($config, $node, $content)}
		{$end}
	</div>
};

(:~
 : a tooltip to go with bootstrap - material design
:)
declare function pmf:tooltip($config as map(*), $node as element(), $class as xs:string+, $content, $title as xs:string?, $placement as xs:string?) {
	let $placement := if ($placement) then
		$placement
	else
		'top'
	return
		<span
			data-toggle="tooltip"
			data-placement="{$placement}"
			title="{$title}"
			class="mouseover {$class}"
			data-html="true">
			{pmf:apply-children($config, $node, $content)}
		</span>
};
(: -------------------------------------------------------------------------------------- :)
(:    New hisoma functions                                                               :)
(: -------------------------------------------------------------------------------------- :)

(:~
 : gets a bibliographic entry via a the zotero api of an inline group library
:)
(: function for ptr @target = content :)
declare function pmf:zotero-entry($config as map(*), $node as element(), $class as xs:string+, $content, $style as xs:string?) {
	let $prefix := $config:bibl-ns || ':'
	let $style := if ($style) then
		$style
	else
		'apa'
	let $ref-entry := substring-after($content, ':')
	let $url := 'https://api.zotero.org/groups/' || $config:zotero-groupid || '/' || 'items?key=' || $config:zotero-key || '&amp;tag=' || $ref-entry || '&amp;format=json&amp;style=' || $style ||
	'&amp;include=citation'
	let $json := json-doc($url)
	let $zotero-entry :=
	for $entry in $json?*
	let $item := $entry?citation
	return
		substring-after(substring-before($item, ')&lt;/span&gt;'), '&lt;span&gt;(')
	return
		<span
			class="{$class}">
			{$zotero-entry},
		</span>
};

(: -------------------------------------------------------------------------------------- :)
(:    New app custom functions                                                               :)
(: -------------------------------------------------------------------------------------- :)




(:~
 : creates bootstrap button to ask for toggle
 :)
declare function pmf:button-toggle($config as map(*), $node as node(), $class as xs:string+, $content, $collapse as xs:string) {
	let $href := '#' || $collapse
	return
		<a
			class="btn btn-primary"
			role="button"
			data-toggle="collapse"
			href="{$href}"
			aria-expanded="false"
			aria-controls="{$collapse}">
			{pmf:apply-children($config, $node, $content)}
		</a>
};

(:~
 : creates bootstrap div with an id
 :)
declare function pmf:block-with-id($config as map(*), $node as node(), $class as xs:string+, $content, $id as xs:string?) {
	<div
		id="{$id}"
		class="{$class}">{pmf:apply-children($config, $node, $content)}</div>
};
