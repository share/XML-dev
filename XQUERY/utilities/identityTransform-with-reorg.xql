xquery version "3.0";
(:~
 : This function returns a deep copy of the elements and all sub-elements
 : (identity transform with typeswitch)
 : copies the input to the output without modification
 : @source http://en.wikibooks.org/wiki/XQuery/Typeswitch_Transformations via Emmanuel Chateau-Dutier https://gist.github.com/emchateau/8ff86d495a9cd3253f99
 :
 : for iglouvre: reorganize child nodes order (skos:narrower at the end)
 :)
declare namespace rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

import module namespace xmldb="http://exist-db.org/xquery/xmldb";

declare variable $input := doc('epivoc_rdf.xml')/rdf:RDF; 
declare variable $collection := '/db/apps/iglouvre-data/data/scripts';
declare variable $file-name := 'epivoc_rdf_corr.xml';

declare function local:copy($input as item()*) as item()* {
  for $node in $input
  return typeswitch($node)
   
    case element() return 
        element {name($node)} {
            (: output each attribute in this element :)
            for $att in $node/@*
                return attribute {name($att)} {$att},
            (: output all the sub-elements of this element recursively :)
            for $child in $node[not(name()='skos:narrower')]
                return 
                (
                    local:copy($child/node()),
                    local:copy($child[name()='skos:narrower']/node()
                )
          }
      (: otherwise pass it through.  Used for text(), comments, and PIs :)
      default return $node
};

let $result :=
    local:copy($input)

return 
    xmldb:store($collection, $file-name, $result)