xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";
(: 
 : Lister tous les éléments distincts de TEI/text
 : pour lister par string-join, essayer : fn:string-join($doc//*/('<' || name(.) || '>, '))
   :)

let $doc := fn:doc("/db/apps/review-examples/data/Beaumont_Clarice.xml")

let $tags := $doc//tei:text//*
let $unique-tags := distinct-values($tags/name())
return
   <result>   
   
      <items>
      {
         for $tag in $unique-tags
         order by $tag
         return <item>{$tag}</item>
      }
      </items>
   
   </result>