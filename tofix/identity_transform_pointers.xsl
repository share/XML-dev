﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="tei xs xsl" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Feb 2019</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: Karnak Cachette</xd:p>
			<xd:p>Modification: 26 February 2019</xd:p>
			<xd:p>Object: correct missiong '#' on data pointers attributes.</xd:p>
			<xd:p>Status: DOESNT WORK</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:*[not(@corresp or @target or @ana or @ref)]">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:layout | tei:decoNote | tei:surface | tei:texpart | tei:locus | tei:msItem">
		<xsl:copy>
			<xsl:variable name="corresp" select="@corresp"/>
			<xsl:apply-templates select="@*[not(local-name() = 'corresp')] | node()"/>
			<xsl:attribute name="corresp">
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$corresp"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:layout | tei:decoNote | tei:surface | tei:texpart | tei:locus | tei:msItem">
		<xsl:copy>
			<xsl:variable name="target" select="@target"/>
			<xsl:apply-templates select="@*[not(local-name() = 'target')] | node()"/>
			<xsl:attribute name="corresp">
				<xsl:text>#</xsl:text>
				<xsl:value-of select="$target"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
