<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:functx="http://www.functx.com" exclude-result-prefixes="xs tei functx" version="3.0">
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<!--  -->
	<!-- TODO : terminer structure work-types shakespeare avec code table -->
	<!-- <code-table xml:id="access-condition-type-code">
	<code-table-name>work-types</code-table-name>
	<basis> liste </basis> -->
	<!-- #EM todo : voir si choses à ajouter de la base cachette -->
	
	<!--<item>
		<id>CK22</id>
		<label>Statue agenouillée théophore (Osiris)</label>
		<value>Corpus saïte</value>
		<idno type="source">http://www.ifao.egnet.net/bases/cachette/?id=22</idno>
		<idno type="uri" subtype="tm">https://www.trismegistos.org/text/701227</idno>
		<bibl type="source">Goyon, 2003</bibl>
		<bibl type="source">Jansen-Winkeln:2014, p.JWIS IV, 1070 (Nr. 60.582)</bibl>
	</item>-->
	<!--
		<cell n="1">CK</cell>
		<cell n="2">fiche word</cell>
		<cell n="3">révision texte</cell>
		<cell n="4">liste titres</cell>
		<cell n="5">Tm (nam-id)</cell>
		<cell n="6">conforme BDD IFAO</cell>
		<cell n="7">balises Xefee</cell>
		<cell n="8">export XML</cell>
		<cell n="9">etat au 1/2/19</cell>
		<cell n="10">export janvier 19</cell>
		<cell n="11">fichiers export Xefee dans Gitlab</cell>
		<cell n="12">Apparat critique nécessaire</cell>
		<cell n="13">Apparat précision</cell>
		<cell n="14">sous corpus</cell>
		<cell n="15">titre court</cell>
		<cell n="16">trismegistos ID</cell>
		<cell n="17">TM TexRef Id</cell>
		<cell n="18">Sources du texte&#xD;</cell>
	
	-->
	<!--  -->
	<xsl:template match="/">
		<code-table xml:id="access-condition-type-code">
			<xsl:comment>Fichier sur le modèle du site exist-db shakespeare avec alimentation de données de la cachette de Karnak depuis ckdata.xml</xsl:comment>
			<code-table-name>work-types</code-table-name>
			<basis> liste </basis>			
			<description></description>
		<xsl:apply-templates select="//tei:table"/>
		</code-table>
	</xsl:template>
	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="//tei:table">
		<items>
			<xsl:apply-templates/>
		</items>
	</xsl:template>
	<xsl:template match="//tei:row">
		<item>
			<xsl:apply-templates/>
		</item>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='1']">
		<id>
			<xsl:value-of select="concat('CK',.)"/>
		</id>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='2']">
		<ficheWord>
			<xsl:apply-templates/>
		</ficheWord>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='3']">
		<revisionTexte>
			<xsl:apply-templates/>
		</revisionTexte>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='4']">
		<listeTitres>
			<xsl:apply-templates/>
		</listeTitres>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='5']">
		<tmNamid>
			<xsl:apply-templates/>
		</tmNamid>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='6']">
		<conformeBddIfao>
			<xsl:apply-templates/>
		</conformeBddIfao>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='7']">
		<balisesXefee>
			<xsl:apply-templates/>
		</balisesXefee>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='8']">
		<exportXml>
			<xsl:apply-templates/>
		</exportXml>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='9']">
		<etatFev19>
			<xsl:apply-templates/>
		</etatFev19>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='10']">
		<exportXefeeJan19>
			<xsl:apply-templates/>
		</exportXefeeJan19>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='11']">
		<fichiersDansGitlab>
			<xsl:apply-templates/>
		</fichiersDansGitlab>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='12']">
		<apparatCritiqueSiNecessaire>
			<xsl:apply-templates/>
		</apparatCritiqueSiNecessaire>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='13']">
		<apparatPrécision>
			<xsl:apply-templates/>
		</apparatPrécision>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='14']">
		<!-- pour le sous corpus sur l'appli existdb -->
		<value>
			<xsl:apply-templates/>
		</value>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='15']">
		<!-- pour le titre court sur l'appli existdb -->
		<label>
			<xsl:apply-templates/>
		</label>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='16']">
		<!-- pour l'identifiant du texte -->
		<idno>
			<xsl:attribute name="type" select="'uri'"/>
			<xsl:attribute name="subtype" select="'tm'"/>
			<xsl:value-of select="concat('https://www.trismegistos.org/text/', .)"/>
		</idno>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='17']">
		<tmId>
			<xsl:apply-templates/>
		</tmId>
	</xsl:template>
	<xsl:template match="//tei:row/tei:cell[@n='18']">
		<bibl>
			<xsl:attribute name="type" select="'source'"/>
			<xsl:apply-templates/>
		</bibl>
	</xsl:template>
</xsl:stylesheet>
