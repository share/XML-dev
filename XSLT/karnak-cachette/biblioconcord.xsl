<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:functx="http://www.functx.com" exclude-result-prefixes="xs tei functx" version="3.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<!--  -->
	<!--<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>-->
	<!--  -->
	<!--<xsl:template match="tei:listBibl">
		<list xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:for-each select="//tei:note[@type = 'tags']">
				<item>
					<xsl:value-of select="."/>
				</item>
			</xsl:for-each>
		</list>
	</xsl:template>-->
	<xsl:template match="/">
		<list>
			<xsl:apply-templates select="//note"/>
		</list>
	</xsl:template>
	<xsl:template match="*/text()"/>
	<xsl:function name="functx:substring-after-last" as="xs:string" xmlns:functx="http://www.functx.com">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:param name="delim" as="xs:string"/>
		<xsl:sequence select="replace($arg, concat('^.*', functx:escape-for-regex($delim)), '')"/>
	</xsl:function>
	<xsl:function name="functx:escape-for-regex" as="xs:string" xmlns:functx="http://www.functx.com">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:sequence select="replace($arg, '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))', '\\$1')"/>
	</xsl:function>
	<xsl:template match="note[@type = 'tags'][starts-with(., '_ck:')]">
		<xsl:for-each select="note">
			<item>
				<insc>
					<xsl:value-of select="."/>
				</insc>
				<zotid>
					<xsl:value-of select="functx:substring-after-last(ancestor::biblStruct[1]/@corresp, '/')"/>
				</zotid>
			</item>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
