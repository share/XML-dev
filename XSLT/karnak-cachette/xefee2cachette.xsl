﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="tei xs" version="2.0">
	<!-- mise en garde : xpath default namespace empêche recours à work-titles qui n'est pas en TEI -->
	<!-- todo -->
	<!-- identifiants  sur membres equipe à faire plus tard (teiCorpus) -->
	<!-- pour l'interface @key epidoc modifier -->
	<!-- tableau work-types avec identifiants trismegistos -->
	<xsl:variable name="works" select="document('work-types.xml')" as="document-node()"/>
	<xsl:key name="TMID" match="//item" use="id"/>
	<xsl:variable name="bibliography" select="document('biblioconcord.xml')" as="document-node()"/>
	<xsl:key name="BIBLIO" match="//item" use="insc"/>
	<xsl:variable name="baseifao" select="tei:publicationStmt/tei:idno[1]"/>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<!--  -->
	<xsl:param name="id" select="tei:TEI/@xml:id"/>
	<xsl:param name="egylang" select="'egy-x-egt-latn'"/>
	<xsl:param name="date-conversion" select="'2019-02-06'"/>
	<!--  -->
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<!--<xsl:template match="*/@xml:lang">
		<xsl:copy>
			<xsl:choose>
				<xsl:when test="contains(., 'eg-tr')">
					<xsl:value-of select="'egy-x-egt-latn'"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:copy>-->
	<!--</xsl:template>-->
	<!-- suppressions -->
	<xsl:template match="@xml:space"/>
	<xsl:template match="TEI/@xml:lang"/>
	<xsl:template match="tei:publicationStmt/tei:idno[1]"/>
	<xsl:template match="tei:language[@ident = 'eg-tr']"/>
	<!--  -->
	<xsl:template match="tei:altIdentifier">
		<xsl:choose>
			<xsl:when test="tei:idno[@type = 'RT'] eq 'RT '"/>
			<xsl:when test="tei:idno[@type = 'CG'] eq 'CG '"/>
			<xsl:when test="tei:idno[@type = 'SR'] eq 'SR '"/>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@* | node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="tei:authority">
		<xsl:copy-of select="."/>
		<authority xmlns="http://www.tei-c.org/ns/1.0">IFAO</authority>
	</xsl:template>
	<xsl:template match="tei:idno[@type = 'URI'][2]/text()">
		<xsl:variable name="id">
			<xsl:value-of select="ancestor::tei:TEI/@xml:id"/>
		</xsl:variable>
		<xsl:variable name="tmid">
			<xsl:value-of select="key('TMID', $id, $works)//idno[@type = 'uri'][@subtype = 'tm']"/>
		</xsl:variable>
		<xsl:if test="not($tmid = '')">
			<!--<xsl:value-of select="."/>
			<xsl:text>/</xsl:text>-->
			<xsl:value-of select="$tmid"/>
		</xsl:if>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:sourceDesc/tei:bibl">
		<!-- todo -->
		<listBibl xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:comment>
				<xsl:text>To refactor ==> Pattern:</xsl:text>
				<xsl:text>				
				</xsl:text>
				<xsl:text>	
					&lt;bibl&gt;
						&lt;ptr target="ZOTEROID"/&gt;
						&lt;citedRange&gt;p. 105, n° 11 (Transcription hiéroglyphes / Traduction)&lt;/citedRange&gt;
					&lt;/bibl&gt;</xsl:text>
				<xsl:text>				
				</xsl:text>
				
			</xsl:comment>
			<!--<xsl:for-each select="key('TMID', $id, $works)//*[@type = 'source']">
				<xsl:element name="bibl" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:if test="starts-with(., 'http://www.')">
						<xsl:attribute name="type" select="'database'"/>
					</xsl:if>
					<xsl:value-of select="."/>
				</xsl:element>
			</xsl:for-each>-->
			<bibl type="type-to-define">
				<xsl:value-of select="key('TMID', $id, $works)//bibl[@type = 'source']"/>
			</bibl>
		</listBibl>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:ptr">
		<!-- replace ptr with  -->
		<xsl:variable name="targetid">
			<xsl:analyze-string select="@target" regex="(#)(.[^,]+)(,*)(.[^,]+)-(\d{{4}})">
				<xsl:matching-substring>
					<xsl:text>_ck:</xsl:text>
					<xsl:value-of select="lower-case(regex-group(2))"/>
					<xsl:value-of select="lower-case(regex-group(3))"/>
					<xsl:value-of select="regex-group(4)"/>
					<xsl:text>:</xsl:text>
					<xsl:value-of select="regex-group(5)"/>
				</xsl:matching-substring>
				<xsl:non-matching-substring>
					<xsl:text>!!!!XXX!!!!</xsl:text>
				</xsl:non-matching-substring>
			</xsl:analyze-string>
		</xsl:variable>
		<xsl:variable name="cleantarget">
			<xsl:value-of select="lower-case(translate($targetid, ' ', ''))"/>
		</xsl:variable>
		<xsl:variable name="zoterotarget">
			<xsl:value-of select="key('BIBLIO', $cleantarget, $bibliography)/zotid"/>
		</xsl:variable>
		<xsl:variable name="target">
			<xsl:choose>
				<xsl:when test="$zoterotarget = ''">
					<xsl:value-of select="substring-after($cleantarget, '_')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$zoterotarget"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:copy>
			<xsl:attribute name="target" select="$target"/>
		</xsl:copy>
	</xsl:template>
	<!-- see tei:lb-->
	<xsl:template match="tei:cb">
		<xsl:variable name="num" select="@n"/>
		<xsl:comment> - - Line  <xsl:value-of select="@n"/>- -</xsl:comment>
		<xsl:variable name="orientation">
			<xsl:choose>
				<xsl:when test="@rend = 'right-to-left'">
					<xsl:text>writing-mode:t-to-b; text-direction:r-to-l;</xsl:text>
				</xsl:when>
				<xsl:when test="@rend = 'left-to-right'">
					<xsl:text>writing-mode:t-to-b; text-direction:l-to-r;</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<lb xmlns="http://www.tei-c.org/ns/1.0" style="{$orientation}" n="{$num}"/>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:change">
		<xsl:variable name="date" select="@when"/>
		<xsl:copy>
			<xsl:attribute name="who" select="'#SQ'"/>
			<xsl:attribute name="when" select="$date"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:date[parent::tei:edition]">
		<xsl:variable name="date">
			<xsl:analyze-string select="." regex="(201)([3-8])">
				<xsl:matching-substring>
					<xsl:value-of select="concat(regex-group(1), 9)"/>
				</xsl:matching-substring>
			</xsl:analyze-string>
		</xsl:variable>
		<date xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:value-of select="$date"/>
		</date>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:dimensions">
		<xsl:copy>
			<xsl:attribute name="type" select="'object'"/>
			<xsl:if test="tei:height != ''">
				<xsl:apply-templates select="tei:height"/>
			</xsl:if>
			<xsl:if test="tei:width != ''">
				<xsl:apply-templates select="tei:width"/>
			</xsl:if>
			<xsl:if test="tei:depth != ''">
				<xsl:apply-templates select="tei:depth"/>
			</xsl:if>
			<xsl:if test="tei:dim != ''">
				<xsl:apply-templates select="tei:dim"/>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:div[@type = 'edition']">
		<div xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="type" select="'edition'"/>
			<xsl:attribute name="xml:lang" select="$egylang"/>
			<xsl:apply-templates select="node()"/>
		</div>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:div[@type = 'textpart']">
		<div xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="xml:id" select="concat($id, '-textpart-', position())"/>
			<xsl:attribute name="type" select="'textpart'"/>
			<xsl:attribute name="corresp">
				<xsl:value-of select="concat('#', $id, '-msitem-', position())"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="concat('#', $id, '-surface-', position())"/>
			</xsl:attribute>
			<!-- @n to the title that will be displayed before the block  -->
			<xsl:attribute name="n" select="'to complete with block title'"/>
			<xsl:comment>@corresp points on surface and msItem</xsl:comment>
			<xsl:comment>Complete @n with block title to be displayed</xsl:comment>
			<xsl:apply-templates select="node()"/>
		</div>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:div[@type = 'apparatus']">
		<div xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="type" select="'apparatus'"/>
			<xsl:text>				
			</xsl:text>
			<xsl:comment>Add apparatus notes.</xsl:comment>
			<xsl:text>				
			</xsl:text>
		</div>
	</xsl:template>
	<!--  -->
	<!--  -->
	<xsl:template match="tei:editionStmt">
		<xsl:copy>
			<xsl:apply-templates/>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Traduction</resp>
				<persName ref="#SQ">
					<forename>Sepideh</forename>
					<surname>Qaheri</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Balisage TEI</resp>
				<persName ref="#SQ">
					<forename>Sepideh</forename>
					<surname>Qaheri</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Balisage TEI</resp>
				<persName ref="#VC">
					<forename>Vincent</forename>
					<surname>Chollier</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Création du modèle de document EpiDoc</resp>
				<persName ref="#EM">
					<forename>Emmanuelle</forename>
					<surname>Morlock</surname>
				</persName>
				<persName ref="#VR">
					<forename>Vincent</forename>
					<surname>Razanajao</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Conversions XSLT</resp>
				<persName ref="#EM">
					<forename>Emmanuelle</forename>
					<surname>Morlock</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Interface de publication</resp>
				<persName resp="#EM">
					<forename>Emmanuelle</forename>
					<surname>Morlock</surname>
				</persName>
			</respStmt>
			<respStmt xmlns="http://www.tei-c.org/ns/1.0">
				<resp>Validation</resp>
				<persName ref="#LC">
					<forename>Laurent</forename>
					<surname>Coulon</surname>
				</persName>
			</respStmt>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:facsimile">
		<xsl:variable name="correp-atts">
			<xsl:call-template name="makeLayoutFromTexparts"/>
		</xsl:variable>
		<xsl:copy>
			<xsl:comment>
				<xsl:text>					
				</xsl:text>
				Structurer les élements graphic par élément surface en reprenant comme identifiant les valeurs définies pour les attributs @corresp des elements layout. 				
				Exemple : &lt;surface xml:id="CK22-surface-basetop" ana="#identifiant-localisation"&gt; (dans le profiledesc du teiCorpus.
				<xsl:text>					
				</xsl:text>
			</xsl:comment>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:gap/@unit">
		<xsl:choose>
			<xsl:when test=". = 'cadrat'">
				<xsl:attribute name="unit">quadrates</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:support//tei:item">
		<xsl:choose>
			<xsl:when test="contains(., 'coudes')">
				<dimensions unit="cm" type="elbow" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:analyze-string select="." regex="(.)*(\d+)(\s*cm)(.)*">
						<xsl:matching-substring>
							<width unit="cm">
								<xsl:variable name="val" select="regex-group(2)"/>
								<xsl:attribute name="quantity" select="$val"/>
								<xsl:value-of select="$val"/>
							</width>
						</xsl:matching-substring>
						<xsl:non-matching-substring>
							<xsl:comment>to verify: <xsl:value-of select="."/></xsl:comment>
						</xsl:non-matching-substring>
					</xsl:analyze-string>
				</dimensions>
			</xsl:when>
			<xsl:when test="contains(., 'base')">
				<dimensions unit="cm" type="base" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:analyze-string select="." regex="(.*)(\d+\.*\d+)(\s*cm)(.*)">
						<xsl:matching-substring>
							<height unit="cm">
								<xsl:variable name="val" select="regex-group(2)"/>
								<xsl:attribute name="quantity" select="$val"/>
								<xsl:value-of select="$val"/>
							</height>
						</xsl:matching-substring>
						<xsl:non-matching-substring>
							<xsl:comment>to verify: <xsl:value-of select="."/></xsl:comment>
						</xsl:non-matching-substring>
					</xsl:analyze-string>
				</dimensions>
			</xsl:when>
			<!--<xsl:otherwise>
				<xsl:comment>to verify</xsl:comment>
				<xsl:value-of select="."/>
			</xsl:otherwise>-->
			<xsl:otherwise/>
		</xsl:choose>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:langUsage">
		<langUsage xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates/>
			<language ident="egy-x-egt-latn" xml:lang="en" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:text>Ancient Egyptian (in the so-called 'Egytien de tradition' stage), rendered in latin script</xsl:text>
			</language>
			<language ident="egy-x-egt-latn" xml:lang="fr" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:text>ancien égyptien dit 'égyptien de tradition' en caractères latins.</xsl:text>
			</language>
		</langUsage>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:layoutDesc">
		<xsl:copy>
			<summary xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:comment>					
					<xsl:text>to add: free text description of general text layout.</xsl:text>					
				</xsl:comment>
			</summary>
			<xsl:comment>
					<xsl:text>						
					</xsl:text>to add: Free text description of the layout
					<xsl:text>						
					</xsl:text>Example: Le texte du pilier dorsal occupe toute la surface du pilier, base exclue et est réparti sur deux colonnes.<xsl:text>						
					</xsl:text>Example: Deux lignes d'inscription devant les genoux du personnage.
					<xsl:text>						
					</xsl:text>
					</xsl:comment>
			<xsl:call-template name="makeLayoutFromTexparts"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="makeLayoutFromTexparts">
		<xsl:for-each select="//tei:div[@type = 'textpart'][tei:ab]">
			<xsl:variable name="counter">
				<xsl:number count="." level="single"/>
			</xsl:variable>
			<xsl:variable name="localisation">
				<xsl:value-of select="concat(@subtype, @n)"/>
			</xsl:variable>
			<layout xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:attribute name="xml:id">
					<xsl:value-of select="concat($id, '-layout-', $counter)"/>
				</xsl:attribute>
				<xsl:attribute name="corresp">
					<xsl:value-of select="concat($id, '-', $localisation)"/>
				</xsl:attribute>
			</layout>
		</xsl:for-each>
	</xsl:template>
	<!--  -->
	<!-- see also tei:cb -->
	<xsl:template match="tei:lb">
		<xsl:variable name="num" select="@n"/>
		<xsl:comment> - - Line  <xsl:value-of select="@n"/>- -</xsl:comment>
		<xsl:variable name="orientation">
			<xsl:choose>
				<xsl:when test="@rend = 'right-to-left'">
					<xsl:text>text-direction:r-to-l;</xsl:text>
				</xsl:when>
				<xsl:when test="@rend = 'left-to-right'">
					<xsl:text>text-direction:l-to-r;</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<lb xmlns="http://www.tei-c.org/ns/1.0" style="{$orientation}" n="{$num}"/>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:licence">
		<licence xmlns="http://www.tei-c.org/ns/1.0" target="http://creativecommons.org/licenses/by/4.0/">Cette œuvre est mise à disposition selon les termes de la <ref xmlns="http://www.tei-c.org/ns/1.0" target="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</ref>.</licence>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:material">
		<xsl:copy>
			<xsl:attribute name="ref" select="'http://thot.philo.ulg.ac.be/concept/XXXXX'"/>
			<xsl:comment>to add: identifier</xsl:comment>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:msDesc">
		<xsl:copy>
			<xsl:apply-templates select="tei:msIdentifier"/>
			<msContents xmlns="http://www.tei-c.org/ns/1.0">
				<summary>
					<xsl:comment>General overview in 1 or 2 sentences.</xsl:comment>
				</summary>
				<xsl:for-each select="//tei:div[@type = 'textpart'][tei:ab]">
					<xsl:variable name="counter">
						<xsl:number count="." level="single"/>
					</xsl:variable>
					<xsl:variable name="localisation">
						<xsl:value-of select="concat(@subtype, @n)"/>
					</xsl:variable>
					<msItem>
						<xsl:attribute name="xml:id">
							<xsl:value-of select="concat($id, '-msitem-', $counter)"/>
						</xsl:attribute>
						<xsl:attribute name="class">
							<xsl:text>#to-define</xsl:text>
						</xsl:attribute>
						<xsl:attribute name="n">
							<xsl:value-of select="$counter"/>
						</xsl:attribute>
						<locus>
							<xsl:attribute name="target">
								<xsl:value-of select="concat($id, '-textpart-', $counter, ' surface-', $localisation)"/>
							</xsl:attribute>
						</locus>
						<title type="modern">
							<xsl:comment>to complete</xsl:comment>
						</title>
						<textLang mainLang="egy-x-egt-latn"/>
					</msItem>
				</xsl:for-each>
			</msContents>
			<xsl:apply-templates select="tei:physDesc"/>
			<xsl:apply-templates select="tei:history"/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:msIdentifier">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
			<msName xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:analyze-string select="//tei:titleStmt/tei:title" regex="^(\w+)(.)">
					<xsl:matching-substring>
						<xsl:value-of select="regex-group(1)"/>
					</xsl:matching-substring>
				</xsl:analyze-string>
				<xsl:text>. Caire </xsl:text>
				<xsl:value-of select="tei:idno[@type = 'JE']"/>
			</msName>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:principal/tei:name">
		<!-- pour principal -->
		<persName xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="ref" select="'#LC'"/>
			<xsl:call-template name="makeStructuredName"/>
		</persName>
	</xsl:template>
	<xsl:template name="makeStructuredName">
		<xsl:analyze-string select="." regex="(\w+)\s(\w+)">
			<xsl:matching-substring>
				<forename xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:value-of select="regex-group(1)"/>
				</forename>
				<forename xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:value-of select="regex-group(2)"/>
				</forename>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="."/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:support/tei:note[not(@type = 'commentaire_dimension')]">
		<xsl:apply-templates select=".//tei:item"/>
	</xsl:template>
	<!-- -->
	<xsl:template match="tei:support/tei:objectType">
		<xsl:copy>
			<xsl:attribute name="ref" select="'completeWithUri'"/>
			<xsl:comment>to complete with the most specific term in the project's vocabulary</xsl:comment>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:support/tei:objectType[1]">
		<xsl:copy>
			<xsl:attribute name="ref" select="'http://vocab.getty.edu/aat/300047600'"/>
			<xsl:value-of select="'Statue'"/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:persName">
		<xsl:variable name="lang">
			<xsl:choose>
				<xsl:when test="@xml:lang = 'eg-tr'">
					<xsl:value-of select="$egylang"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="@xml:lang"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:copy>
			<xsl:if test="not(@xml:lang = 'fr') and ancestor-or-self::tei:particDesc">
				<xsl:attribute name="type" select="'attested'"/>
			</xsl:if>
			<xsl:text>				
			</xsl:text>
			<xsl:comment>Add tm URI to @ref</xsl:comment>
			<xsl:text>				
			</xsl:text>
			<xsl:comment>Add tm canonic name to @nymRef</xsl:comment>
			<xsl:text>				
			</xsl:text>
			<name xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:if test="@xml:lang">
					<xsl:attribute name="xml:lang" select="$lang"/>
				</xsl:if>
				<xsl:value-of select="normalize-space(.)"/>
			</name>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="tei:respStmt/tei:persName">
		<xsl:variable name="initials">
			<xsl:analyze-string select="." regex="([A-Z])(.+)\s([A-Z])(.+)">
				<xsl:matching-substring>
					<xsl:value-of select="concat(regex-group(1), regex-group(3))"/>
				</xsl:matching-substring>
			</xsl:analyze-string>
		</xsl:variable>
		<xsl:copy>
			<xsl:attribute name="ref" select="concat('#', $initials)"/>
			<xsl:call-template name="makeStructuredName"/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:person">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
			<xsl:comment>Add TM id for the person in @sameAs</xsl:comment>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:physDesc">
		<xsl:copy>
			<xsl:apply-templates/>
			<decoDesc xmlns="http://www.tei-c.org/ns/1.0">
				<decoNote>
					<xsl:attribute name="xml:id">
						<xsl:value-of select="concat($id, '-deconote-1')"/>
					</xsl:attribute>
					<xsl:attribute name="corresp">
						<xsl:value-of select="concat($id, '-surface-id-1')"/>
					</xsl:attribute>
					<xsl:text>							
						</xsl:text>
					<xsl:comment>						
						<xsl:text>
						</xsl:text>
						<xsl:text>To add: one decoNote for each iconography, decorative features or other imagery.</xsl:text>
						<xsl:text>	
							
						</xsl:text>
						<xsl:text>Example - Figure de la déesse &lt;persName type="divine"&gt; &lt;name&gt;Isis&lt;/name&gt; &lt;/persName&gt;. &lt;desc&gt;Elle tient un sceptre-ouas dans la main gauche et une croix-ânkh dans la main droite. Elle est coiffée du disque solaire avec uraeus, enserré dans les deux cornes.</xsl:text>	
						<xsl:text>
						</xsl:text>
					</xsl:comment>
				</decoNote>
			</decoDesc>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:ref[parent::tei:edition]">
		<title xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:value-of select="."/>
		</title>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:respStmt">
		<xsl:copy>
			<resp xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:text>Translittération</xsl:text>
			</resp>
			<xsl:apply-templates select="tei:persName"/>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:rs[@type = 'person']">
		<rs xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:variable name="ref" select="@ref"/>
			<xsl:attribute name="ref" select="concat('#', $ref)"/>
			<xsl:attribute name="type" select="'person'"/>
			<xsl:apply-templates/>
		</rs>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:rs[@type = 'title']">
		<roleName xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="type" select="'title'"/>
			<xsl:apply-templates/>
		</roleName>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:titleStmt/tei:title">
		<xsl:variable name="idno">
			<xsl:analyze-string select="." regex="(\.)\s(Caire\sJE\s\d+)">
				<xsl:matching-substring>
					<xsl:value-of select="regex-group(2)"/>
				</xsl:matching-substring>
			</xsl:analyze-string>
		</xsl:variable>
		<xsl:copy>
			<xsl:apply-templates/>
			<xsl:text> </xsl:text>
			<idno type="JE" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:value-of select="$idno"/>
			</idno>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:revisionDesc">
		<xsl:copy>
			<listChange xmlns="http://www.tei-c.org/ns/1.0">
				<change xmlns="http://www.tei-c.org/ns/1.0" who="#EM" when="{$date-conversion}">XSLT convertion to a new document model.</change>
				<!-- copie de tous les enfants -->
				<!--<xsl:copy-of select="./*"/>-->
				<xsl:apply-templates/>
			</listChange>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<xsl:template match="tei:rs[@type = 'statPreserv']">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
			<xsl:comment>to add: @ref with uri for the preservation state, eagle or other (getty?)</xsl:comment>
		</xsl:copy>
	</xsl:template>
	<!--  -->
	<!-- deletes last part of the title -->
	<xsl:template match="tei:titleStmt/tei:title/text()">
		<xsl:value-of select="normalize-space(substring-before(., 'Caire'))"/>
	</xsl:template>
	<!--  -->
</xsl:stylesheet>
