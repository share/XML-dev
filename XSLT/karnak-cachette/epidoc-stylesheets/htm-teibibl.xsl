<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.tei-c.org/ns/1.0" xmlns:f="http://example.com/ns/functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:html="http://www.w3.org/1999/html" exclude-result-prefixes="t f html math" version="3.0">
	<!-- #EM pas compris pourquoi dû ajouter html des exclude result prefixes -->
	<!--

Pietro notes on 14/8/2015 work on this template, from mail to Gabriel.

- I have converted the TEI bibliography of IRT and IGCyr to ZoteroRDF 
(https://github.com/EAGLE-BPN/BiblioTEI2ZoteroRDF) in this passage I have tried to 
distinguish books, bookparts, articles and conference proceedings.

- I have uploaded these to the zotero eagle open group bibliography 
(https://www.zotero.org/groups/eagleepigraphicbibliography)

- I have created a parametrized template in my local epidoc xslts which looks at the json 
and TEI output of the Zotero api basing the call on the content of ptr/@target in each 
bibl. It needs both because the key to build the link is in the json but the TEI xml is 
much more accessible for the other data. I tried also to grab the html div exposed in the 
json, which would have been the easiest thing to do, but I can only get it escaped and 
thus is not usable.
** If set on 'zotero' it prints surname, name, title and year with a link to the zotero 
item in the eagle group bibliography. It assumes bibl only contains ptr and citedRange.
** If set on 'localTEI' it looks at a local bibliography (no zotero) and compares the 
@target to the xml:id to take the results and print something (in the sample a lot, but 
I'd expect more commonly Author-Year references(.
** I have also created sample values for irt and igcyr which are modification of the 
zotero option but deal with some of the project specific ways of encoding the 
bibliography. All examples only cater for book and article.



-->
	<!--
		
		Pietro Notes on 10.10.2016
		
		this should be modified based on parameters to
		
		* decide wheather to use zotero or a local version of the bibliography in TEI
	
		* assuming that the user has entered a unique tag name as value of ptr/@target, decide group or user in zotero to look up based on parameter value entered at transformation time
	
		* output style based on Zotero Style Repository stored in a parameter value entered at transformation time
		
		
	
	-->
	<!--<xsl:template match="t:citedRange">
		<xsl:param name="parm-leiden-style" tunnel="yes" required="no"/>
		<xsl:param name="parm-bib" tunnel="yes" required="no"/>
		<xsl:param name="parm-bibloc" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroUorG" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroKey" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroNS" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroStyle" tunnel="yes" required="no"/>
		
		<xsl:value-of select="concat('uorg:',$parm-zoteroUorG,' ns:',$parm-zoteroNS,' style:',$parm-zoteroStyle)"/>
	</xsl:template>-->
	<xsl:template match="t:bibl" priority="1">
		<xsl:param name="parm-bib" tunnel="yes" required="no"/>
		<xsl:param name="parm-bibloc" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroUorG" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroKey" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroNS" tunnel="yes" required="no"/>
		<xsl:param name="parm-zoteroStyle" tunnel="yes" required="no"/>
		<xsl:choose>
			<!-- default general zotero behaviour prints 
				author surname and name, title in italics, date and links to the zotero item page on the zotero bibliography. 
				assumes the inscription source has no free text in bibl, 
				!!!!!!!only a <ptr target='key'/> and a <citedRange>pp. 45-65</citedRange>!!!!!!!
			it also assumes that the content of ptr/@target is a unique tag used in the zotero bibliography as the ids assigned by Zotero are not
			reliable enough for this purpose according to Zotero forums.
			
			if there is no ptr/@target, this will try anyway and take a lot of time.
			-->
			<xsl:when test="$parm-bib = 'none'">
				<xsl:apply-templates/>
			</xsl:when>
			<xsl:when test="$parm-bib = 'zotero'">
				<xsl:choose>
					<!-- check if there is a ptr at all
					
					WARNING. if the pointer is not there, the transformation will simply stop and return a premature end of file message e.g. it cannot find what it is looking for via the zotero api
					-->
					<xsl:when test=".[t:ptr]">
						<!-- check if a namespace is provided for tags/xml:ids and use it as part of the tag for zotero-->
						<xsl:variable name="biblentry" select="
								if ($parm-zoteroNS) then
									concat($parm-zoteroNS, ./t:ptr/@target)
								else
									./t:ptr/@target"/>
						<!-- get the full data in json -->
						<xsl:variable name="zoteroapibase" select="'https://api.zotero.org/' || $parm-zoteroUorG || '/' || $parm-zoteroKey || '/' || 'items?tag='"/>
						<xsl:variable name="zoteroapijson">
							<xsl:value-of select="$zoteroapibase || $biblentry || '&amp;format=json&amp;style=' || $parm-zoteroStyle || '&amp;include=data,citation'"/>
						</xsl:variable>
						<xsl:variable name="zoteroapibib">
							<xsl:value-of select="$zoteroapibase || $biblentry || '&amp;format=bib&amp;style=' || $parm-zoteroStyle"/>
						</xsl:variable>
						<xsl:variable name="unparsedtext" select="unparsed-text($zoteroapijson)"/>
						<!-- html wrapper elements depending on the t:bibl context -->
						<xsl:choose>
							<xsl:when test="parent::t:listBibl[ancestor::t:sourceDesc]">
								<li>
									<xsl:call-template name="makeEntry">
										<xsl:with-param name="unparsedtext" select="$unparsedtext"/>
										<xsl:with-param name="zoteroStyle" select="$parm-zoteroStyle"/>
										<xsl:with-param name="bib" select="$zoteroapibib"/>
										<xsl:with-param name="typeReference" select="'short'"/>
									</xsl:call-template>
									<xsl:if test="not(position() = last())">
										<xsl:value-of select="' ; '"/>
									</xsl:if>
								</li>
							</xsl:when>
							<xsl:when test="ancestor::t:div[@type = 'apparatus' or @type = 'commentary' or @type = 'bibliography']">
								<xsl:call-template name="makeEntry">
									<xsl:with-param name="unparsedtext" select="$unparsedtext"/>
									<xsl:with-param name="zoteroStyle" select="$parm-zoteroStyle"/>
									<xsl:with-param name="bib" select="$zoteroapibib"/>
									<xsl:with-param name="typeReference" select="'short'"/>
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:call-template name="makeEntry">
									<xsl:with-param name="unparsedtext" select="$unparsedtext"/>
									<xsl:with-param name="zoteroStyle" select="$parm-zoteroStyle"/>
									<xsl:with-param name="bib" select="$zoteroapibib"/>
									<xsl:with-param name="typeReference" select="'long'"/>
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<!-- if there is no ptr, print simply what is inside bibl and a warning message-->
					<xsl:otherwise>
						<xsl:text>toto</xsl:text>
						<xsl:value-of select="."/>
						<xsl:message>There is no ptr with a @target in the bibl element <xsl:copy-of select="."/>. A target equal to a tag in your zotero bibliography is necessary.</xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<!--uses the local TEI bibliography at the path specified in parameter parm-bibloc -->
			<xsl:when test="$parm-bib = 'localTEI'">
				<xsl:variable name="biblentry" select="./t:ptr/@target"/>
				<xsl:variable name="biblentryID" select="substring-after(./t:ptr/@target, '#')"/>
				<!--					parameter localbibl should contain the path to the bibliography relative to this xslt -->
				<xsl:variable name="textref" select="document(string($parm-bibloc))//t:bibl[@xml:id = $biblentryID]"/>
				<xsl:for-each select="$biblentry">
					<xsl:choose>
						<!--this will print a citation according to the selected style with a link around it pointing to the resource DOI, url or zotero item view-->
						<xsl:when test="not(ancestor::t:div[@type = 'bibliography'])">
							<!-- basic	render for citations-->
							<xsl:choose>
								<xsl:when test="$textref/@xml:id = $biblentryID">
									<xsl:choose>
										<xsl:when test="$textref//t:author">
											<xsl:value-of select="$textref//t:author[1]"/>
											<xsl:if test="$textref//t:author[2]">
												<xsl:text>-</xsl:text>
												<xsl:value-of select="$textref//t:author[2]"/>
											</xsl:if>
											<xsl:text>, </xsl:text>
										</xsl:when>
										<xsl:when test="$textref//t:editor">
											<xsl:value-of select="$textref//t:editor[1]"/>
											<xsl:if test="$textref//t:editor[2]">
												<xsl:text>-</xsl:text>
												<xsl:value-of select="$textref//t:editor[2]"/>
											</xsl:if>
										</xsl:when>
									</xsl:choose>
									<xsl:text> (</xsl:text>
									<xsl:value-of select="$textref//t:date"/>
									<xsl:text>), </xsl:text>
									<xsl:value-of select="$textref//t:biblScope"/>
								</xsl:when>
								<xsl:otherwise>
									<!--if this appears the id do not really correspond to each other, 
									ther might be a typo or a missing entry in the bibliography-->
									<xsl:message>
										<xsl:text> there is no entry in your bibliography file at </xsl:text>
										<xsl:value-of select="$parm-bibloc"/>
										<xsl:text> with the @xml:id</xsl:text>
										<xsl:value-of select="$biblentry"/>
										<xsl:text>!</xsl:text>
									</xsl:message>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<!--						rudimental render for each entry in bibliography-->
							<xsl:choose>
								<xsl:when test="$textref/@xml:id = $biblentryID">
									<xsl:value-of select="$textref"/>
									<!--assumes a sligthly "formatted" bibliography...-->
								</xsl:when>
								<xsl:otherwise>
									<!--if this appears the id do not really correspond to each other, 
									ther might be a typo or a missing entry in the bibliography-->
									<xsl:message>
										<xsl:text> there is no entry in your bibliography file at </xsl:text>
										<xsl:value-of select="$parm-bibloc"/>
										<xsl:text> for the entry </xsl:text>
										<xsl:value-of select="$biblentry"/>
										<xsl:text>!</xsl:text>
									</xsl:message>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<!-- This applyes other templates and does not call the zotero api -->
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="makeEntry">
		<xsl:param name="unparsedtext"/>
		<xsl:param name="zoteroStyle"/>
		<xsl:param name="typeReference"/>
		<xsl:param name="bib"/>
		<!-- variables  -->
		<xsl:variable name="url" select="parse-json($unparsedtext)?*?data?url"/>
		<xsl:variable name="doi" select="parse-json($unparsedtext)?*?data?DOI"/>
		<xsl:variable name="zoteroitemurl" select="parse-json($unparsedtext)?*?links?self?href"/>
		<xsl:variable name="zoterocitation" select="parse-json($unparsedtext)?*?citation"/>
		<xsl:variable name="completeref">
			<xsl:copy-of select="document($bib)/div"/>
		</xsl:variable>
		<!-- either doi, fulltext or zotero item from original zotero collection -->
		<xsl:variable name="pointerurl">
			<xsl:choose>
				<xsl:when test="$doi">
					<xsl:value-of select="$doi"/>
				</xsl:when>
				<xsl:when test="$url">
					<xsl:value-of select="$url"/>
				</xsl:when>
				<xsl:when test="$zoteroitemurl">
					<xsl:value-of select="$zoteroitemurl"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'#'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!--		<xsl:variable name="test" select="'&lt;span&gt;(Pernigotti 2014)&lt;/span&gt;'"/>-->
		<!-- extract citation without the parenthesis of the style  -->
		<xsl:variable name="shortref">
			<xsl:choose>
				<!-- delete enclosing parenthesis in chicago citations -->
				<xsl:when test="contains($zoteroStyle, 'chicago')">
					<xsl:analyze-string select="$zoterocitation" regex="(&lt;span&gt;)([(])(.+)([)])(&lt;[/]span&gt;)">
						<xsl:matching-substring>
							<xsl:value-of select="regex-group(3)"/>
						</xsl:matching-substring>
					</xsl:analyze-string>
				</xsl:when>
				<xsl:otherwise>
					<xsl:analyze-string select="$zoterocitation" regex="(&lt;span&gt;)(.+)(&lt;[/]span&gt;)">
						<xsl:matching-substring>
							<xsl:value-of select="regex-group(2)"/>
						</xsl:matching-substring>
					</xsl:analyze-string>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="title">
			<xsl:choose>
				<xsl:when test="$typeReference = 'long'">
					<xsl:value-of select="$shortref"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$completeref"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="reference">
			<xsl:choose>
				<xsl:when test="$typeReference = 'short'">
					<xsl:choose>
						<xsl:when test="normalize-space($zoteroitemurl) = ''">
							<xsl:text>[lien manquant]</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$shortref"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="t:*">
						<xsl:text>,</xsl:text>
					</xsl:if>
					<xsl:apply-templates/>
				</xsl:when>
				<xsl:when test="normalize-space($zoteroitemurl) = ''">
					<xsl:text>[lien manquant]</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$completeref"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="normalize-space($zoteroitemurl) = ''">
				<xsl:value-of select="$reference"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="makeReference">
					<xsl:with-param name="url" select="$pointerurl"/>
					<xsl:with-param name="title" select="$title"/>
					<xsl:with-param name="reference" select="normalize-space($reference)"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="makeReference">
		<xsl:param name="url"/>
		<xsl:param name="title"/>
		<xsl:param name="reference"/>
		<span class="refbibl">
			<a href="{$url}" data-toggle="tooltip" title="{$title}" data-placement="bottom" class="ck-tooltip">
				<xsl:value-of select="$reference"/>
			</a>
		</span>
	</xsl:template>
</xsl:stylesheet>
