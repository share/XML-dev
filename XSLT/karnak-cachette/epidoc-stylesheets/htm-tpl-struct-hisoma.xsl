<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:functx="http://www.functx.com" exclude-result-prefixes="t xs functx" version="2.0">
	<!-- créé à partir de htm-tpl-struct-iospe.xsl -->
	<!-- Contains named templates for Hisoma file structure (aka "metadata" aka "supporting data") -->
	<!-- Called from htm-tpl-structure.xsl -->
	<!-- Formatted with bootstrap 3 (see W3C schools) -->
	<xsl:template name="hisoma-body-metadata">
		<div id="monument" class="table-responsive">
			<h3>Monument</h3>
			<table class="table table-condensed">
				<tbody>
					<!-- Corps du tableau -->
					<tr>
						<td>Type de monument</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:support//t:objectType//text()">
									<xsl:apply-templates select="//t:support//t:objectType"/>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Matériau</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:support//t:material//text()">
									<xsl:apply-templates select="//t:support//t:material"/>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Dimensions</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:support//t:dimensions//text()">
									<ul class="list-unstyled">
										<xsl:apply-templates select="//t:support//t:dimensions"/>
									</ul>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Etat de conservation</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:support//t:rs[@type = 'statPreserv']//text()">
									<xsl:value-of select="//t:support//t:rs[@type = 'statPreserv']"/>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Date</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:origin/t:origDate/text()">
									<xsl:apply-templates select="//t:origin/t:origDate"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Lieu d'origine</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:origin/t:origPlace/text()">
									<xsl:apply-templates select="//t:origin/t:origPlace"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Lieu de découverte</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:provenance[@type = 'found']//text()">
									<xsl:value-of select="//t:provenance[@type = 'found']"/>
								</xsl:when>
								<xsl:otherwise>-</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Contexte de la découverte</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:provenance[@type = 'context']//text()">
									<xsl:value-of select="//t:provenance[@type = 'context']"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Lieu de conservation</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:msIdentifier//t:repository/text() and //t:msIdentifier//t:idno/text()">
									<xsl:value-of select="//t:msIdentifier//t:repository"/>
									<xsl:if test="//t:msIdentifier/t:settlement/text()">
										<xsl:text> - </xsl:text>
										<xsl:value-of select="//t:msIdentifier/t:settlement"/>
									</xsl:if>
									<xsl:choose>
										<!-- if both, put altIdentifiers between parenthesis -->
										<xsl:when test="//t:msIdentifier/t:altIdentifier/t:idno/text() and //t:msIdentifier/t:idno/text()">
											<br/>
											<xsl:text>Numéro d'inventaire : </xsl:text>
											<xsl:apply-templates select="//t:msIdentifier/t:idno"/>
											<xsl:if test="//t:msIdentifier/t:altIdentifier/t:idno/text()">
												<br/>
												<small>
													<xsl:text>Autre(s) numéro(s) : </xsl:text>
													<xsl:apply-templates select="//t:msIdentifier/t:altIdentifier/t:idno"/>
												</small>
											</xsl:if>
										</xsl:when>
										<xsl:otherwise> - </xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="//t:msIdentifier/t:repository/text()">
									<xsl:value-of select="//t:msIdentifier//t:repository"/>
									<xsl:text>Pas de numéro d'inventaire</xsl:text>
								</xsl:when>
								<xsl:otherwise>Inconnu</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="inscriptions" class="table-responsive">
			<h3>Inscriptions</h3>
			<table class="table table-condensed">
				<tbody>
					<tr>
						<td>Textes</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:msContents//text()">
									<xsl:apply-templates select="//t:msContents"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Disposition</td>
						<td>
							<xsl:if test="//t:layoutDesc/t:summary//text()">
								<xsl:value-of select="//t:layoutDesc/t:summary"/>
								<br/>
							</xsl:if>
							<xsl:choose>
								<xsl:when test="//t:layoutDesc/count(//t:layout/text()) = 1">
									<xsl:apply-templates select="//t:layoutDesc"/>
								</xsl:when>
								<xsl:when test="//t:layoutDesc/count(//t:layout/text()) > 1">
									<xsl:apply-templates select="//t:layoutDesc" mode="list"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Mains</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:handNote//text()[not(ancestor::t:height)]">
									<xsl:value-of select="/t:handNote//text()[not(ancestor::t:height)]"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Scripts</td>
						<td>
							<xsl:choose>
								<xsl:when test="//scriptNote//text()">
									<xsl:value-of select="/scriptNote//text()"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Iconographie</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:layoutDesc/count(//t:decoNote//text()) = 1">
									<xsl:apply-templates select="//t:decoDesc"/>
								</xsl:when>
								<xsl:when test="//t:layoutDesc/count(//t:decoNote//text()) > 1">
									<xsl:apply-templates select="//t:decoDesc" mode="list"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Personnages mentionnés ou représentés</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:particDesc//text()">
									<xsl:apply-templates select="//t:particDesc"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
					<tr>
						<td>Formules</td>
						<td>
							<xsl:call-template name="formula-summary"/>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div id="digitalEdition" class="table-responsive">
			<h3>Edition électronique</h3>
			<table class="table table-condensed">
				<tbody>
					<tr>
						<td>Editeur scientifique</td>
						<td>
							<xsl:apply-templates select="//t:editionStmt//t:respStmt[t:resp = 'Translittération']//t:persName"/>
						</td>
					</tr>
					<tr>
						<td>Responsable scientifique du projet</td>
						<td>
							<xsl:apply-templates select="//t:editionStmt//t:principal//t:persName"/>
						</td>
					</tr>
					<tr>
						<td>Modèle de données TEI / EpiDoc</td>
						<td>
							<xsl:apply-templates select="//t:editionStmt//t:respStmt[t:resp[@key = 'epidoc']]//t:persName"/>
						</td>
					</tr>
					<tr>
						<td>Texte établi à partir de :</td>
						<td>
							<xsl:choose>
								<xsl:when test="//t:sourceDesc/t:listBibl/text()">
									<!--<xsl:text>Texte établi à partir de :</xsl:text>-->
									<xsl:apply-templates select="//t:sourceDesc/t:listBibl" mode="inline-list"/>
								</xsl:when>
								<xsl:when test="//t:sourceDesc/t:bibl/text()">
									<xsl:apply-templates select="//t:sourceDesc/t:bibl"/>
								</xsl:when>
								<xsl:otherwise> - </xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</xsl:template>
	<!-- use one of these -->
	<xsl:template name="hisoma-body-edition-collapse">
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-target="#collapse-edition" href="#collapse-edition">Texte</a>
					</h4>
				</div>
				<div id="collapse-edition" class="panel-collapse collapse in">
					<div class="panel-body">
						<div>
							<!-- Edited text output -->
							<xsl:variable name="edtxt">
								<xsl:apply-templates select="//t:div[@type = 'edition']"/>
							</xsl:variable>
							<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
							<xsl:apply-templates select="$edtxt" mode="sqbrackets"/>
						</div>
						<!-- utiliser epidoc stylesheets -->
						<div id="apparatus">
							<!-- todo : utiliser styles epidoc -->
							<hr/>
							<h6>Apparat critique</h6>
							<br/>
							<p>
								<!-- Apparatus text output -->
								<xsl:variable name="apptxt">
									<xsl:apply-templates select="//t:div[@type = 'apparatus']/t:listApp"/>
								</xsl:variable>
								<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
								<xsl:apply-templates select="$apptxt" mode="sqbrackets"/>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-target="#collapse-translation" href="#collapse-translation">Traduction</a>
					</h4>
				</div>
				<div id="collapse-translation" class="panel-collapse collapse">
					<div class="panel-body">
						<!-- Translation text output -->
						<xsl:variable name="transtxt">
							<!--<xsl:apply-templates select="//t:div[@type = 'translation']//t:p"/>-->
							<xsl:apply-templates select="//t:div[@type = 'translation']"/>
						</xsl:variable>
						<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
						<xsl:apply-templates select="$transtxt" mode="sqbrackets"/>
					</div>
				</div>
			</div>
			<xsl:if test="//t:div[@type = 'commentary']//t:p/text()">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-target="#collapse-commentary" href="#collapse-commentary">Commentaire</a>
						</h4>
					</div>
					<div id="collapse-commentary" class="panel-collapse collapse">
						<div class="panel-body">
							<!-- Commentary text output -->
							<xsl:variable name="commtxt">
								<xsl:apply-templates select="//t:div[@type = 'commentary']//t:p"/>
							</xsl:variable>
							<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
							<xsl:apply-templates select="$commtxt" mode="sqbrackets"/>
						</div>
					</div>
				</div>
			</xsl:if>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle" data-toggle="collapse" data-target="#collapse-bibliography" href="#collapse-bibliography">Bibliographie</a>
					</h4>
				</div>
				<div id="collapse-bibliography" class="panel-collapse collapse">
					<div class="panel-body">
						<xsl:apply-templates select="//t:body//t:div[@type = 'bibliography']//t:bibl"/>
					</div>
				</div>
			</div>
		</div>
	</xsl:template>
	<!--  -->
	<xsl:template name="hisoma-body-edition-tabs">
		<xsl:param name="id"/>
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#edition-{$id}">Texte</a>
			</li>
			<li>
				<a data-toggle="tab" href="#translation-{$id}">Traduction</a>
			</li>
			<li>
				<a data-toggle="tab" href="#commentary-{$id}">Commentaire</a>
			</li>
			<li>
				<a data-toggle="tab" href="#bibliography-{$id}">Bibliographie</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="edition-{$id}" class="tab-pane fade in active">
				<!-- Edited text output -->
				<xsl:variable name="edtxt">
					<xsl:apply-templates select="//t:div[@type = 'edition']"/>
				</xsl:variable>
				<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
				<xsl:apply-templates select="$edtxt" mode="sqbrackets"/>
				<div id="apparatus">
					<!-- Apparatus text output -->
					<xsl:variable name="apptxt">
						<xsl:apply-templates select="//t:div[@type = 'apparatus']/t:listApp"/>
					</xsl:variable>
					<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
					<xsl:apply-templates select="$apptxt" mode="sqbrackets"/>
				</div>
			</div>
			<div id="translation-{$id}" class="tab-pane fade">
				<!-- Translation text output -->
				<xsl:variable name="transtxt">
					<!--<xsl:apply-templates select="//t:div[@type = 'translation']//t:p"/>-->
					<xsl:apply-templates select="//t:div[@type = 'translation']"/>
				</xsl:variable>
				<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
				<xsl:apply-templates select="$transtxt" mode="sqbrackets"/>
			</div>
			<div id="commentary-{$id}" class="tab-pane fade">
				<!-- Commentary text output -->
				<xsl:variable name="commtxt">
					<xsl:apply-templates select="//t:div[@type = 'commentary']//t:p"/>
				</xsl:variable>
				<!-- Moded templates found in htm-tpl-sqbrackets.xsl -->
				<xsl:apply-templates select="$commtxt" mode="sqbrackets"/>
			</div>
			<div id="bibliography-{$id}" class="tab-pane fade">
				<xsl:apply-templates select="//t:body//t:div[@type = 'bibliography']/t:listBibl"/>
			</div>
		</div>
	</xsl:template>
	<xsl:template name="hisoma-structure">
		<xsl:variable name="title">
			<xsl:call-template name="hisoma-title"/>
		</xsl:variable>
		<html lang="fr">
			<!--  https://www.w3schools.com/booTsTrap/tryit.asp?filename=trybs_temp_webpage&stacked=h -->
			<head>
				<meta charset="utf-8"/>
				<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
				<title>
					<xsl:value-of select="$title"/>
				</title>
				<!-- bootstrap -->
				<link href="../bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet"/>
				<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>-->
					<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
					<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
					<!--[if lt IE 9]>
      				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    				<![endif]-->
					
				<!-- googlefonts -->
				<link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i|Oswald|Roboto" rel="stylesheet"/>
				<!-- Found in htm-tpl-cssandscripts.xsl -->
				<xsl:call-template name="css-script"/>
				<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"/>-->
				<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"/>-->
				<style>
					/* Remove the navbar's default margin-bottom and rounded borders */
					.navbar {
						margin-bottom: 0;
						border-radius: 0;
					}
					
					/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
					.row.content {
						height: 450px
					}
					
					/* Set gray background color and 100% height */
					.sidenav {
						padding-top: 20px;
						xbackground-color: #f1f1f1;
						height: 100%;
					}
					
					/* Set black background color, white text and some padding */
					footer {
						background-color: #555;
						color: white;
						padding: 15px;
					}
					
					/* On small screens, set height to 'auto' for sidenav and grid */
					@media screen and (max-width : 767px) {
						.sidenav {
							height: auto;
							padding: 15px;
						}
						.row.content {
							height: auto;
						}
					}
					/* colonnes de hauteur égale cf. https://codepen.io/chriscoyier/pen/fHowu */
					
					#layout {
						background: linear-gradient(
					to right, 
					
					#d0d0d0, 
					#d0d0d0 20%, 
					
					#f6f6f6 20%, 
					#f6f6f6 80%, 
					
					#64a1d8 80%, 
					#64a1d8
					);
					}
					
					.sidenav {
						float: left;
						width: 20%;
						padding: 20px;
						text-align: left;
					}
					
					.sidenav {
						float: left;
						width: 20%;
						padding: 20px;
						text-align: left;
					}
					
					.main {
						float: left;
						width: 60%;
						padding: 20px;
					}
					
					* {
						box-sizing: border-box;
					}
					
					.jumbotron {
						background: #ffff4c
					}
					
					#accordion {
						background: #fff
					}
					
					/* adjustments to the EpiDoc example stylesheets */
					.textpart {
						left: 4em;
						margin-right: 4em;
						line-height: 1.6;
						font-family: 'Noto Serif', serif;
						position: relative;
					}
					#translation p {
						left: 4em;
						margin-right:4em;
						line-height: 1.6;
						position: relative;
					}
					#apparatus {
						left: 4em;
						line-height: 1.6;
						font-size: small;
						position: relative;
						width: 95%;
					}
					#apparatus-inline {
						left: 4em;
						line-height: 1.6;
						font-style: normal;
						font-size: small;
						display: block;
						position: relative;
						width: 95%;
					}
					#apparatus-inline hr,
					#apparatus hr {
						border-top: 3px double #ddd;
						margin-top: 5em;
					}
					table td:nth-child(1) {
						width: 20%;
						max-width: 20%;
					}
					
					/* typography */
					table {
						font-size: small;
					}
					h1 {
						font-size: 1.8em !important;
					}
					h2 {
						font-size: 1.8em !important;
					}
					h3 {
						font-size: 1.5em !important;
					}
					h4 {
						font-size: 1.3em !important;
					}
					h5 {
						font-size: 1.2em !important;
						margin-top:2em !important;
					}
					h6 {
						font-size: 1.1em !important;
					}
					h1,
					h2,
					h3,
					h4,
					h5,
					h6 {
						font-family: 'Oswald', sans-serif;
						font-style: normal;
					}
					
					h5 {
						border-bottom: 1px solid #ddd;
						padding-top: 3em;
					}
					h5:nth-child(1)
					{
						padding-top: 0;
					}
					.ck-line-number {
						left: -4em;
						padding-right: 0.2em;
						position: absolute;
						font-style: normal;
						font-family: 'Roboto', sans-serif;
						color: #666;
					}
					#accordion .btn-default {
						color: #333;
					}
					#accordion p {
						margin-bottom: 0 4em 1em 0;
					}
					#accordion {
						overflow-wrap: break-word;
					}
					#apparatus p,
					#commentary p {margin-right:4em;}
					/* ne marche pas */
					.ck-tooltip + .tooltip.bottom .tooltip-inner {
						background-color: #666;
						padding: 1.3em;
					}
					.ck-tooltip + .tooltip.bottom .tooltip-arrow {
						border-top-color: #666;
					}
					/**/
					/*
					.tooltip-inner {
						max-width: none;
						white-space: nowrap;
					}*/
					/* from https://stackoverflow.com/questions/14596743/how-do-you-change-the-width-and-height-of-twitter-bootstraps-tooltips */
					.tooltip-inner {
						min-width: 100px;
						max-width: 400px;
						color:#ffffb3; // même couleur que le background du lien
						letter-spacing: 5rem !important;
						
					}
					
					@media (max-width : 320px) {
						.tooltip-inner {
							min-width: initial;
							max-width: 320px;
						}
					}
					
					body {
						font-family: 'Roboto', sans-serif;
					}
					.caption span {
						display: block;
					}
					
					a,
					a:link,
					a:visited,
					a:hover,
					a:active {
						text-decoration: none;
						font-size: inherit;
						font-family: inherit;
					}
					
					a:hover {
						background-color: #ffffb3;
					}
					
					.panel-body a,
					.panel-body a:link,
					.panel-body a:visited,
					.panel-body a:hover,
					.panel-body a:active {
					
						color: #cc3399;
					}
					
					.refbibl {
						display: block;
						margin-bottom: 15px;
					}
					.list-inline .refbibl,
					#apparatus .refbibl,
					#commentary .refbibl {
						display: inline;
					}
					dl-horizontal dt {
						width: 100px !important;
					}
					dl-horizontal dd {
						margin-left: 100px !important;
					}
					.fixed {
						position: fixed;
					}
					
					*[lang ^= "egy-"] {
						font-style: italic;
					}
					*[lang = "fr"] {
						font-style: normal;
					}
					.note {
						font-style: normal;
					}
					
					.loc {
						color: #666;
					}
					#apparatus p .loc span {
						color: black;					
					}</style>
			</head>
			<body>
				<!-- nav -->
				<nav class="navbar navbar-inverse">
					<div class="container-fluid">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"/>
								<span class="icon-bar"/>
								<span class="icon-bar"/>
							</button>
							<!--<a class="navbar-brand" href="#">Logo</a>-->
						</div>
						<div class="collapse navbar-collapse" id="myNavbar">
							<ul class="nav navbar-nav">
								<li>
									<a href="http://epidoc.sourceforge.net">Accueil EpiDoc</a>
								</li>
								<li>
									<a href="http://www.stoa.org/epidoc/gl/latest/">EpiDoc - Guidelines</a>
								</li>
								<li>
									<a href="https://wiki.digitalclassicist.org/EpiDoc_Workshops#Training_Materials">Epidoc - Supports de formation</a>
								</li>
								<li>
									<a href="http://thot.philo.ulg.ac.be/">Thot Thesauri and Ontologies</a>
								</li>
								<li>
									<a href="http://thesaurus.mom.fr/">Thesaurus MOM</a>
								</li>
								<li>
									<a href="https://www.trismegistos.org/">Trismegistos</a>
								</li>
								<li>
									<a href="https://pleiades.stoa.org/">Pleiades</a>
								</li>
								<li>
									<a href="http://www.ifao.egnet.net/bases/cachette/">Base cachette de Karnak</a>
								</li>
							</ul>
							<!--<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="#"><span class="glyphicon glyphicon-log-in"/> Login</a>
								</li>
							</ul>-->
						</div>
					</div>
				</nav>
				<!-- end nav -->
				<!-- content -->
				<div id="layout" class="container-fluid text-center">
					<div class="row content">
						<!-- col left -->
						<div class="col-sm-2 sidenav">
							<div class="row gallery text-center">
								<xsl:apply-templates select="//t:graphic" mode="image-gallery"/>
							</div>
						</div>
						<!-- col center -->
						<div class="col-sm-8 text-left main">
							<div class="row">
								<div class="col-sm-12">
									<div class="jumbotron">
										<h1>
											<xsl:value-of select="$title"/>
										</h1>
									</div>
									<!--<h2>Métadonnées</h2>-->
									<xsl:call-template name="hisoma-body-metadata"/>
									<!--<h2>Inscriptions</h2>-->
									<xsl:call-template name="hisoma-body-edition-collapse"/>
								</div>
							</div>
							<!--<h2>Edition</h2>
							<div class="row">
								<div class="col-sm-6" style="border:1px solid #ccc;">
									<xsl:call-template name="hisoma-body-edition-collapse">
										<xsl:with-param name="id" select="'l'"/>
									</xsl:call-template>
								</div>
								<div class="col-sm-6" style="border:1px solid #ccc;">
									<xsl:call-template name="hisoma-body-edition">
										<xsl:with-param name="id" select="'r'"/>
									</xsl:call-template>
								</div>
							</div>-->
						</div>
						<!-- col right -->
						<div class="col-sm-2 sidenav">
							<div class="well fixed" style="top: 20%;">
								<h2>Atteindre</h2>
								<div class="btn-group-vertical">
									<a href="#accordion" class="btn btn-default" role="button">Texte</a>
									<a href="#monument" class="btn btn-default" role="button">Metadonnées</a>
									<!--<a href="#translation" class="btn btn-info" role="button">Link Button</a>
									<a href="#bibliography" class="btn btn-info" role="button">Link Button</a>-->
								</div>
							</div>
							<div class="well fixed" style="bottom: 5%;">
								<h2>Surligner</h2>
								<div class="list-group">
									<div class="checkbox">
										<label><input type="checkbox" value="formula"/>Formule</label>
										<!-- rs[@type='formula'] -->
										<!-- span.formula -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="person"/>Personne</label>
										<!-- rs[@type='person'] -->
										<!-- span.person -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="persname"/>Nom de personne</label>
										<!-- persName -->
										<!-- span.persName -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="persname-divine"/>Nom de divinité</label>
										<!-- persName[@type='divine'] -->
										<!-- span.persname-divine -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="epithet"/>Epithète</label>
										<!-- addName[@type='epithet'] -->
										<!-- span.epithet -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="epithet-divine"/>Epithète divine</label>
										<!-- addName[@type='epithet-divine'] -->
										<!-- span.epithet-divine -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="rolename-title"/>Titre</label>
										<!-- roleName[@type='title'] -->
										<!-- span.rolename-title -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="filiationmark"/>Marque de filiation</label>
										<!-- seg[@type='filiationMark'] -->
										<!-- span.filiationmark -->
									</div>
									<div class="checkbox">
										<label><input type="checkbox" value="placename"/>Lieu</label>
										<!-- placeName -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- footer -->
				<footer class="container-fluid text-center">
					<p>&#xA9; 2018 <xsl:apply-templates select="//t:authority"/></p>
				</footer>
				<script>
					$(document).ready(function(){
					$('[data-toggle="tooltip"]').tooltip();   
					});
				</script>
			</body>
		</html>
	</xsl:template>
	<xsl:template name="hisoma-title">
		<xsl:choose>
			<xsl:when test="//t:titleStmt/t:title/text() and matches(//t:idno[@type = 'filename'], '^\d\.\d{1,4}$')">
				<xsl:number value="substring-before(//t:idno[@type = 'filename'], '.')" format="I"/>
				<xsl:text>&#xa0;</xsl:text>
				<xsl:number value="substring-after(//t:idno[@type = 'filename'], '.')" format="1"/>
				<xsl:text>.&#xa0;</xsl:text>
				<xsl:if test="string(normalize-space(//t:origPlace[1]))">
					<xsl:value-of select="normalize-space(//t:origPlace[1])"/>
					<xsl:text>.&#xa0;</xsl:text>
				</xsl:if>
				<xsl:value-of select="//t:titleStmt/t:title[child::text()][1]"/>
				<xsl:if test="not(//t:titleStmt/t:title[child::text()][1][child::t:origDate])">
					<xsl:text>,&#xa0;</xsl:text>
					<xsl:value-of select="//t:origDate[1]"/>
				</xsl:if>
			</xsl:when>
			<xsl:when test="//t:titleStmt/t:title/text()">
				<xsl:value-of select="//t:titleStmt/t:title"/>
			</xsl:when>
			<xsl:when test="//t:sourceDesc//t:bibl/text()">
				<xsl:value-of select="//t:sourceDesc//t:bibl"/>
			</xsl:when>
			<xsl:when test="//t:idno[@type = 'filename']/text()">
				<xsl:value-of select="//t:idno[@type = 'filename']"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>EpiDoc example output, HiSoMA style</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- pourraient être distribués sous forme de fichiers indépendants -->
	<!-- htm-teilayoutdescandlayout -->
	<xsl:template match="t:layoutDesc | t:decoDesc" mode="list">
		<ul class="list-unstyled">
			<xsl:apply-templates select="t:layout | t:decoNote" mode="list"/>
		</ul>
	</xsl:template>
	<xsl:template match="t:layoutDesc//t:layout" mode="list">
		<li>
			<xsl:apply-templates/>
		</li>
	</xsl:template>
	<xsl:template match="t:decoNote" mode="list">
		<li>
			<xsl:call-template name="tooltip">
				<xsl:with-param name="tooltip-url" select="'#'"/>
				<xsl:with-param name="tooltip-placement" select="'bottom'"/>
				<xsl:with-param name="tooltip-content">
					<xsl:apply-templates select="t:desc"/>
				</xsl:with-param>
				<xsl:with-param name="content">
					<xsl:apply-templates select="." mode="tooltip"/>
				</xsl:with-param>
			</xsl:call-template>
		</li>
	</xsl:template>
	<xsl:template match="t:decoNote">
		<xsl:call-template name="tooltip">
			<xsl:with-param name="tooltip-url" select="'#'"/>
			<xsl:with-param name="tooltip-placement" select="'bottom'"/>
			<xsl:with-param name="tooltip-content">
				<xsl:apply-templates select="t:desc"/>
			</xsl:with-param>
			<xsl:with-param name="content">
				<xsl:apply-templates select="." mode="tooltip"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="tooltip">
		<xsl:param name="tooltip-content"/>
		<xsl:param name="tooltip-url"/>
		<xsl:param name="tooltip-placement"/>
		<xsl:param name="content"/>
		<a href="{$tooltip-url}" data-toggle="tooltip" title="{$tooltip-content}" data-placement="{$tooltip-placement}" class="ck-tooltip">
			<xsl:value-of select="$content"/>
		</a>
	</xsl:template>
	<xsl:template match="t:decoNote//t:desc" mode="tooltip"/>
	<!--  -->
	<xsl:template match="t:idno | t:objectType | t:material | t:origDate | t:origPlace | t:authority | t:persName | t:name">
		<xsl:apply-templates/>
		<xsl:if test="position() != last()">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="t:persName[ancestor::t:listPerson] | t:name[ancestor::t:listPerson]">
		<xsl:apply-templates/>
	</xsl:template>
	<!-- provisoire (remplacé par utilisation des styles zotero de liuzzo) -->
	<!--<xsl:template match="t:bibl/t:ptr">
		<xsl:value-of select="@target"/>
	</xsl:template>-->
	<!-- links -->
	<!-- lien vers les referentiels en ligne -->
	<xsl:template match="t:objectType[@ref] | t:material[@ref] | t:origDate[@period] | t:placeName[@ref]">
		<!-- get identifier -->
		<xsl:variable name="pointer-or-id">
			<xsl:if test="@xml:id">
				<xsl:value-of select="@xml:id"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="@ref">
				<xsl:value-of select="@ref"/>
				<xsl:text> </xsl:text>
			</xsl:if>
			<xsl:if test="@period">
				<xsl:value-of select="@period"/>
				<xsl:text> </xsl:text>
			</xsl:if>
		</xsl:variable>
		<!-- if @ref, bootstrap link -->
		<a href="{@ref|@period}" class="btn btn-info" role="button" title="{$pointer-or-id}">
			<xsl:apply-templates/>
		</a>
	</xsl:template>
	<!-- lien simple -->
	<xsl:template match="t:ref[@ref]">
		<!-- if @ref, bootstrap link -->
		<a href="{@ref}" class="btn btn-link" role="link" title="{@ref}">
			<xsl:apply-templates/>
		</a>
	</xsl:template>
	<!-- dimensions	-->
	<xsl:template match="t:dimensions[@type]">
		<!--		<xsl:text>Dimensions (H. &#xd7; l. &#xd7; pr.)</xsl:text>-->
		<li>
			<xsl:variable name="type" select="@type"/>
			<xsl:variable name="type-fr">
				<xsl:apply-templates select="$type"/>
			</xsl:variable>
			<strong>
				<xsl:value-of select="functx:capitalize-first($type-fr)"/>
			</strong>
			<xsl:text> : </xsl:text>
			<xsl:apply-templates select="t:height | t:width | t:depth | t:dim[@type = 'diameter']"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="@unit"/>
			<!-- s'il y a un commentaire en note avec le même type-->
			<xsl:if test="//t:support//t:note[@type = 'dimensions_note'][@subtype = $type]//text()">
				<xsl:text> (</xsl:text>
				<xsl:value-of select="//t:support//t:note[@type = 'dimensions_note'][@subtype = $type]"/>
				<xsl:text>)</xsl:text>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="t:dimensions/@type">
		<xsl:choose>
			<xsl:when test=". = 'object'">
				<xsl:text>objet</xsl:text>
			</xsl:when>
			<xsl:when test=". = 'elbow'">
				<xsl:text>au coudes</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="."/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="t:height">
		<xsl:text>H. </xsl:text>
		<xsl:value-of select="."/>
		<xsl:if test="following-sibling::*">
			<xsl:text> &#xd7; </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="t:width">
		<xsl:text>l. </xsl:text>
		<xsl:value-of select="."/>
		<xsl:if test="following-sibling::*">
			<xsl:text> &#xd7; </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="t:depth">
		<xsl:text>pr. </xsl:text>
		<xsl:value-of select="."/>
		<xsl:if test="following-sibling::*">
			<xsl:text> &#xd7; </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="t:dim[@type = 'diameter']">
		<xsl:text>diam. </xsl:text>
		<xsl:value-of select="."/>
		<xsl:if test="following-sibling::*">
			<xsl:text> &#xd7; </xsl:text>
		</xsl:if>
	</xsl:template>
	<!-- lien simple -->
	<xsl:template match="t:ref[@target] | t:ptr[@target][not(ancestor::t:div[@type = 'bibliography'])]">
		<!-- if @ref, bootstrap link -->
		<a href="{@ref}" class="btn btn-link" role="link">
			<xsl:apply-templates/>
		</a>
	</xsl:template>
	<!-- varia -->
	<xsl:template match="t:emph">
		<strong>
			<xsl:apply-templates/>
		</strong>
	</xsl:template>
	<!-- functions -->
	<xsl:function name="functx:capitalize-first" as="xs:string?" xmlns:functx="http://www.functx.com">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:sequence select="concat(upper-case(substring($arg, 1, 1)), substring($arg, 2))"/>
	</xsl:function>
	<!-- template htm-teidivedition.xsl -->
	<!-- Textpart div -->
	<xsl:template match="t:div[@type = 'edition']//t:div[@type = 'textpart']" priority="1">
		<xsl:param name="parm-leiden-style" tunnel="yes" required="no"/>
		<xsl:param name="parm-internal-app-style" tunnel="yes" required="no"/>
		<xsl:variable name="div-type">
			<xsl:for-each select="ancestor::t:div[@type != 'edition']">
				<xsl:value-of select="@type"/>
				<xsl:text>-</xsl:text>
			</xsl:for-each>
		</xsl:variable>
		<!-- #EM -->
		<xsl:variable name="subtype">
			<xsl:value-of select="functx:capitalize-first(translate(@subtype, '-', ' '))"/>
		</xsl:variable>
		<!-- end #EM -->
		<xsl:variable name="div-loc">
			<xsl:for-each select="ancestor::t:div[@type = 'textpart'][@n]">
				<xsl:value-of select="@n"/>
				<xsl:text>-</xsl:text>
			</xsl:for-each>
		</xsl:variable>
		<h5>
			<xsl:value-of select="$subtype"/>
		</h5>
		<div class="arrows">
			<!-- warning: based on the first lb of an ab ; supposes all lb of an ab are identical -->
			<xsl:variable name="var">
				<xsl:value-of select="(.//t:*[local-name() = 'lb'])[1]/@style"/>
			</xsl:variable>
			<xsl:call-template name="col-lines-orientations">
				<xsl:with-param name="string" select="$var"/>
			</xsl:call-template>
		</div>
		<!--<xsl:if test="@n"><!-\- prints div number -\->
			<span class="textpartnumber" id="{$div-type}ab{$div-loc}{@n}">
				<!-\- add ancestor textparts -\->
				<xsl:if test="($parm-leiden-style = 'ddbdp' or $parm-leiden-style = 'sammelbuch') and @subtype">
					<xsl:value-of select="@subtype"/>
					<xsl:text> </xsl:text>
				</xsl:if>
				<xsl:value-of select="@n"/>
			</span>
			<xsl:if test="child::*[1][self::t:div[@type='textpart'][@n]]"><br /></xsl:if>
		</xsl:if>-->
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template name="col-lines-orientations">
		<!-- <lb n="1" style="writing-mode:t-to-b; text-direction:l-to-r;"/> -->
		<xsl:param name="string"/>
		<!-- for now, works if all lb of one divpart are identical -->
		<xsl:variable name="tokenized" select="tokenize($string, ';\s')"/>
		<xsl:for-each select="$tokenized">
			<xsl:sort order="descending"/>
			<xsl:variable name="cleaned" select="translate(., ';', '')"/>
			<!-- writing mode first -->
			<xsl:choose>
				<xsl:when test="starts-with($cleaned, 'writing-mode:')">
					<xsl:variable name="column">
						<xsl:value-of select="substring-after($cleaned, 'writing-mode:')"/>
					</xsl:variable>
					<button type="button" class="btn btn-default btn-sm">
						<xsl:element name="span">
							<xsl:attribute name="class">
								<xsl:text>glyphicon glyphicon-arrow-</xsl:text>
								<xsl:choose>
									<xsl:when test="$column = 't-to-b'">
										<xsl:text>down</xsl:text>
									</xsl:when>
									<xsl:when test="$column = 'b-to-t'">
										<xsl:text>up</xsl:text>
									</xsl:when>
								</xsl:choose>
							</xsl:attribute>
						</xsl:element>
					</button>
				</xsl:when>
				<xsl:when test="starts-with($cleaned, 'text-direction:')">
					<xsl:variable name="line">
						<xsl:value-of select="substring-after($cleaned, 'text-direction:')"/>
					</xsl:variable>
					<button type="button" class="btn btn-default btn-sm">
						<xsl:element name="span">
							<xsl:attribute name="class">
								<xsl:text>glyphicon glyphicon-arrow-</xsl:text>
								<xsl:choose>
									<xsl:when test="$line = 'l-to-r'">
										<xsl:text>right</xsl:text>
									</xsl:when>
									<xsl:when test="$line = 'r-to-l'">
										<xsl:text>left</xsl:text>
									</xsl:when>
								</xsl:choose>
							</xsl:attribute>
						</xsl:element>
					</button>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<!-- image gallery -->
	<xsl:template match="t:facsimile//t:graphic" mode="image-gallery">
		<div class="col-md-12">
			<div class="thumbnail">
				<a href="{@url}">
					<img src="{@url}" alt="Lights" style="width:100%"/>
					<div class="caption">
						<p>
							<small>
								<xsl:apply-templates select="t:desc"/>
							</small>
						</p>
					</div>
				</a>
			</div>
		</div>
	</xsl:template>
	<xsl:template match="t:desc">
		<span class="{@type}">
			<xsl:apply-templates/>
			<xsl:text>. </xsl:text>
		</span>
	</xsl:template>
	<xsl:template match="t:desc[@type = 'fonds']"/>
	<xsl:template match="t:desc[@type = 'licence']"/>
	<xsl:template match="t:head">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<xsl:template match="t:listBibl" mode="inline-list">
		<ul class="list-inline">
			<xsl:apply-templates select="t:bibl"/>
		</ul>
	</xsl:template>
	<!-- ****************************************************************
	 
		templates pour faire des span correspondant aux cases à cocher 
	                                           
	    **************************************************************** -->
	<xsl:template match="t:rs">
		<span class="{@type}">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:text//t:addName">
		<xsl:variable name="type">
			<xsl:if test="@type">
				<xsl:value-of select="@type"/>
			</xsl:if>
		</xsl:variable>
		<span class="{$type}">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:seg[@type = 'filiationMark']">
		<span class="filiationmark">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:roleName">
		<xsl:variable name="type">
			<xsl:if test="@type">
				<xsl:text>-</xsl:text>
				<xsl:value-of select="@type"/>
			</xsl:if>
		</xsl:variable>
		<span class="rolename{$type}">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:text//t:persName">
		<xsl:variable name="lang">
			<xsl:choose>
				<xsl:when test="@xml:lang">
					<xsl:value-of select="@xml:lang"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<!--<xsl:when test="ancestor::t:div[@type = 'edition' or @type='translation'][@xml:lang]">
							<xsl:value-of select="ancestor::t:div[@type = 'edition' or @type='translation']/@xml:lang"/>
						</xsl:when>-->
						<xsl:when test="ancestor::t:*[@xml:lang][1]">
							<xsl:value-of select="ancestor::t:*[@xml:lang][1]/@xml:lang"/>
						</xsl:when>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="type">
			<xsl:if test="@type">
				<xsl:text>-</xsl:text>
				<xsl:value-of select="@type"/>
			</xsl:if>
		</xsl:variable>
		<span class="persname{$type}" lang="{$lang}">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:placeName">
		<xsl:variable name="lang">
			<xsl:call-template name="addLang"/>
		</xsl:variable>
		<span class="placename {@type}" lang="{$lang}">
			<xsl:apply-templates/>
		</span>
	</xsl:template>
	<xsl:template match="t:text//t:name[@nymRef]">
		<xsl:call-template name="tooltip">
			<xsl:with-param name="tooltip-url" select="'#'"/>
			<xsl:with-param name="tooltip-placement" select="'bottom'"/>
			<xsl:with-param name="tooltip-content">
				<xsl:apply-templates select="@nymRef"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<!--  -->
	<!-- **************************************************************** -->
	<!--  -->
	<xsl:template name="addLang">
		<xsl:choose>
			<xsl:when test="@xml:lang">
				<xsl:value-of select="@xml:lang"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="ancestor::t:div[@type = 'edition' or @type = 'translation'][@xml:lang]">
						<xsl:value-of select="ancestor::t:div[@type = 'edition' or @type = 'translation']/@xml:lang"/>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="t:listPerson">
		<ul class="list-group">
			<xsl:apply-templates/>
		</ul>
	</xsl:template>
	<xsl:template match="t:person">
		<li class="list-group-item">
			<!-- numéro de personne dans le document -->
			<xsl:variable name="numpersonne">
				<xsl:text>Personne </xsl:text>
				<xsl:value-of select="@n"/>
			</xsl:variable>
			<!-- l'identifiant -->
			<xsl:call-template name="tooltip">
				<xsl:with-param name="tooltip-url" select="'#'"/>
				<xsl:with-param name="tooltip-content" select="@xml:id"/>
				<xsl:with-param name="tooltip-placement" select="'bottom'"/>
				<xsl:with-param name="content" select="$numpersonne"/>
			</xsl:call-template>
			<xsl:text> </xsl:text>
			<!-- sex sign -->
			<xsl:choose>
				<xsl:when test="@sex = 1">
					<span class="sex">&#9794;</span>
				</xsl:when>
				<xsl:when test="@sex = 2">
					<span class="sex">&#9792;</span>
				</xsl:when>
			</xsl:choose>
			<!-- le nom de personne (ref trismegistos en tooltip s'il existe) -->
			<xsl:choose>
				<xsl:when test="@sameAs">
					<!-- s'il y a une personne correspondante dans la base trismegistos -->
					<xsl:variable name="url" select="@sameAs"/>
					<xsl:call-template name="tooltip">
						<xsl:with-param name="tooltip-url" select="$url"/>
						<xsl:with-param name="tooltip-placement" select="'bottom'"/>
						<xsl:with-param name="tooltip-content">
							<xsl:value-of select="$url"/>
						</xsl:with-param>
						<xsl:with-param name="content">
							<xsl:apply-templates/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
			<!-- relations  -->
			<xsl:if test="parent::t:listPerson//t:listRelation/t:relation[@name = 'parent']">
				<xsl:variable name="personId" select="@xml:id"/>
				<xsl:variable name="relation">
					<xsl:value-of select="parent::t:listPerson//t:listRelation/t:relation[@name = 'parent']/local-name(@*[. = concat('#', $personId)])"/>
				</xsl:variable>
				<xsl:text> - </xsl:text>
				<!-- la relation -->
				<xsl:choose>
					<xsl:when test="$relation = 'active'">
						<xsl:choose>
							<xsl:when test="@sex = '1'">
								<xsl:text>père</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>mère</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> de </xsl:text>
						<xsl:text>Personne </xsl:text>
						<xsl:variable name="personne-liee">
							<xsl:value-of select="substring-after(following-sibling::t:listRelation/t:relation/@passive, '#')"/>
						</xsl:variable>
						<xsl:value-of select="parent::t:listPerson/t:person[@xml:id = $personne-liee]/@n"/>
					</xsl:when>
					<xsl:when test="$relation = 'passive'">
						<xsl:choose>
							<xsl:when test="@sex = '1'">
								<xsl:text>fils</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>fille</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:text> de </xsl:text>
						<xsl:variable name="personne-liee">
							<xsl:value-of select="substring-after(following-sibling::t:listRelation/t:relation/@active, '#')"/>
						</xsl:variable>
						<xsl:text>Personne </xsl:text>
						<xsl:value-of select="parent::t:listPerson/t:person[@xml:id = $personne-liee]/@n"/>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:if>
			<!-- role -->
			<xsl:if test="@role">
				<xsl:choose>
					<xsl:when test="@role = 'owner'">
						<br/>
						<xsl:text>Propriétaire de la statue</xsl:text>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="formula-summary">
		<ul class="list-unstyled">
			<xsl:variable name="count">
				<xsl:value-of select="count(//t:div[@type = 'edition']//t:rs[@type = 'formula'])"/>
			</xsl:variable>
			<!--<xsl:choose>
				<xsl:when test="$count >= 1">
					<xsl:value-of select="$count"/>
					<xsl:text> formule</xsl:text>
					<xsl:if test="$count > 1">
						<xsl:text>s</xsl:text>
					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> - </xsl:text>
				</xsl:otherwise>
			</xsl:choose>-->
			<xsl:for-each-group select="//t:div[@type = 'edition']//t:rs[@type = 'formula']" group-by="@subtype">
				<li>
					<xsl:variable name="current">
						<xsl:value-of select="current-grouping-key()"/>
					</xsl:variable>
					<xsl:variable name="label">
						<xsl:choose>
							<xsl:when test="$current = 'saitic'">
								<xsl:text>formule saïte</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$current"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<span class="badge">
						<xsl:value-of select="count(current-group())"/>
					</span>
					<xsl:text> </xsl:text>
					<xsl:value-of select="$label"/>
					<ul>
						<xsl:for-each select="current-group()">
							<li>
								<xsl:call-template name="corresp-translit">
									<xsl:with-param name="corresp">
										<xsl:value-of select="ancestor::t:div[@type = 'textpart'][@xml:id][1]/@xml:id"/>
									</xsl:with-param>
								</xsl:call-template>
							</li>
						</xsl:for-each>
					</ul>
				</li>
			</xsl:for-each-group>
		</ul>
	</xsl:template>
	<!-- textes -->
	<xsl:template match="t:msContents">
		<ul class="list-group">
			<xsl:apply-templates/>
		</ul>
	</xsl:template>
	<xsl:template match="t:msContents/t:summary">
		<li class="list-group-item">
			<xsl:apply-templates/>
		</li>
	</xsl:template>
	<xsl:template match="t:msContents/t:msItem">
		<li class="list-group-item">
			<xsl:text>Texte </xsl:text>
			<xsl:value-of select="@n"/>
			<xsl:text> : </xsl:text>
			<xsl:apply-templates select="t:title[@type = 'modern']"/>
			<xsl:if test="t:textLang">
				<br/>
				<xsl:choose>
					<xsl:when test="t:textLang/text()">
						<xsl:value-of select="t:textLang"/>
					</xsl:when>
					<xsl:when test="t:textLang/@mainLang">
						<xsl:variable name="textLang" select="t:textLang/@mainLang"/>
						<small>
							<xsl:text>Langue principale :</xsl:text>
							<xsl:value-of select="//t:language[@ident = $textLang][@xml:lang = 'fr']"/>
						</small>
					</xsl:when>
				</xsl:choose>
			</xsl:if>
			<!--<xsl:if test="@ana or @class">
				<br/>
				<xsl:text>Genre : </xsl:text>
				<xsl:if test="@ana">
					<xsl:value-of select="@ana"/>
				</xsl:if>
				<xsl:text> - </xsl:text>
				<xsl:if test="@class">
					<xsl:value-of select="@class"/>
				</xsl:if>
			</xsl:if>-->
		</li>
	</xsl:template>
	<!-- voir où le placer -->
	<xsl:template name="corresp-translit">
		<xsl:param name="corresp"/>
		<xsl:variable name="subtype">
			<xsl:value-of select="//t:div[@type = 'edition']//t:div[@type = 'textpart'][@xml:id = $corresp]/@subtype"/>
		</xsl:variable>
		<xsl:value-of select="functx:capitalize-first(translate($subtype, '-', ' '))"/>
	</xsl:template>
	<!--<xsl:function name="functx:substring-before-match" as="xs:string" xmlns:functx="http://www.functx.com">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:param name="regex" as="xs:string"/>
		<xsl:sequence select="tokenize($arg,$regex)[1]"/>
	</xsl:function>-->
</xsl:stylesheet>
