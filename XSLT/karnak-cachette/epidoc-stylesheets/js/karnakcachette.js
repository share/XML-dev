


// highlight sentences w the same class as a moused-over sentence
function highlightWords() {
	
	// 0th step: clear existing highlighting
	var textpart = document.getElementsByTagName("rolename");
	
	for (i = 0; i < textpart.length; i++) {
		textpart[i].style.backgroundColor = document.body.style.backgroundColor;
	}
	
	// first step: get the class of a moused over element (and highlight that element)
	var classes = this.getAttribute('type').split(" ");
	
	// second step: find all other elements, in all other languages, that match that class
	for (i = 0; i < classes.length; i++) {
		var same = document.getElementsByClassName(classes[i])
		for (j = 0; j < same.length; j++) {
			//third step: highlight them!
			same[j].style.backgroundColor = 'olive';
		}
	}
}

// display corpus and document meta-data when the language is moused-over
// a lot of code for this function was taken from David Birnbaum's JS tutorial
// this tutorial can be found here: http://dh.obdurodon.org/javascript/popup/
function showMD(event) {
	//http://answers.oreilly.com/topic/1823-adding-a-page-overlay-in-javascript/
	/*
	 * id: unique id that can be used to close one popup while leaving others open
	 * text: plain text body of popup
	 */
	var popupId = this.dataset.id;
	if (document.getElementById(popupId)) {
		// delete exisiting pop up with a second click
		document.body.removeChild(document.getElementById(popupId));
		return;
	}
	var definition = this.dataset.definition;
	// text processing to make it pretty
	var table = "<table>";
	
	var h = "", d = "";
	var attrs = definition.split(" ");
	for (var i = 0; i < attrs.length; i++) {
		// if it has =, we have come to the next attribute and need to process the last one
		if (attrs[i].includes("=")) {
			table += "<tr><th>" + h + "</th><td>" + d + "</td></tr>";
			h = attrs[i].split("=")[0];
			h = h.replace("-", " ");
			d = attrs[i].split("=")[1];
		} else {
			d += " " + attrs[i];
		}
	}
	
	table += "</table>";
	
	var overlay = document.createElement("div");
	var XMousePos = event.clientX;
	var Ypos = event.clientY + window.pageYOffset;
	var windowWidth = window.innerWidth;
	if (windowWidth - XMousePos > 300) {
		var Xpos = XMousePos
	} else {
		var Xpos = windowWidth - 310
	};
	overlay.setAttribute("id", popupId);
	overlay.setAttribute("style", "z-index: 10; background-color: white; position: absolute; left: " + Xpos + "px; top: " + Ypos + "px; border: 2px solid black; width: 300px; padding: 2px; margin: 0;")
	overlay.setAttribute("onclick", "document.body.removeChild(document.getElementById('" + popupId + "'))");
	overlay.setAttribute("class", "overlay");
	overlay.innerHTML = "<div>" + table + "</div>";
	document.body.appendChild(overlay);
}



var originalBackground;
function toggleHighlight() {
	var pos = this.getAttribute('value');
	var color;
	switch (pos) {
		case 'formula':
		color = '#F9D9D9';
		// rose
		break;
		case 'person':
		color = '#D9F9D9';
		// vert
		break;
		case 'persname':
		color = '#D9D9F9';
		// violet
		break;
		case 'persname-divine':
		color = '#F9F9D9';
		//beige
		break;
		case 'epithet':
		color = '#EBD699';
		// sable
		break;
		case 'epithet-divine':
		color = '#BB969A';
		// sable rose
		break;
		case 'rolename-title':
		color = '#EBEFCD';
		// vert clair
		break;
		case 'filiationmark':
		color = '#D2E6A9';
		// autre vert
		break;
		case 'placename':
		color = '#BFDFEF';
		//bleu
		break;
	}
	var status = this.checked;
	// the status to which you've just changed the checkbox
	var spans = document.getElementsByClassName(pos);
	for (var i = 0; i < spans.length; i++) {
		if (status == true) {
			spans[i].style.backgroundColor = color;
		} else {
			spans[i].style.backgroundColor = originalBackground;
		}
	}
}



function init() {
	//add event listeners
	var textpart = document.getElementsByTagName("rolename");
	
	for (var i = 0; i < textpart.length; i++) {
		textpart[i].addEventListener('mouseover', highlightWords, false);
	}
	
	
	var spans = document.getElementsByTagName("span");
	
	for (var i = 0; i < spans.length; i++) {
		spans[i].addEventListener('click', showMD, false);
	}
	
	originalBackground = document.body.style.backgroundColor;
	var checkboxes = document.getElementsByTagName('input');
	for (var i = 0; i < checkboxes.length; i++) {
		checkboxes[i].addEventListener('click', toggleHighlight, false);
	}
}




window.addEventListener('DOMContentLoaded', init, false);