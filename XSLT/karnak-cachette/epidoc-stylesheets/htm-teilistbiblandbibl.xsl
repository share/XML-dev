<?xml version="1.0" encoding="UTF-8"?>
<!-- $Id$ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:t="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="t" version="2.0">
	<xsl:template match="t:listBibl">
		<xsl:param name="parm-edition-type" tunnel="yes" required="no"/>
		<xsl:choose>
			<xsl:when test="$parm-edition-type = 'hisoma'">
				<p>
					<xsl:text>Texte établi à partir de :</xsl:text>
				</p>
				<ul>
					<xsl:apply-templates select="t:bibl"/>
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<ul>
					<xsl:apply-templates/>
				</ul>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="t:listBibl/t:bibl">
		<li>
			<xsl:apply-templates/>
		</li>
	</xsl:template>
</xsl:stylesheet>
