﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:functx="http://www.functx.com"
	exclude-result-prefixes="tei xs xsl functx" version="2.0">
	<xsl:param name="inputTree" select="doc('../../bibliography/EIAD_bibliography_initials.xml')//tei:list"/>
	<xsl:key name="INITIALS" match="//tei:item" use="substring-after(., 'eiad-bibl:')"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Apr 20, 2017</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emmam_morlock (CNRS)</xd:p>
			<xd:p>Project: EIAD</xd:p>
			<xd:p>Modification: 02 July 2017</xd:p>
			<xd:p>Object: removes underscores in date fields and add final dot to notes and preserves tags (in titles in particular) and adds @key to biblstruct according to xml file.</xd:p>
			<xd:p>Modification: 29 November 2017</xd:p>
			<xd:p>Object: delete empty author or editor + cleaning.</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Delete empty nodes (eg author or editor).</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:author | tei:editor">
		<xsl:choose>
			<xsl:when test="not(string(.))"/>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@* | node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Named template to replaces hash by en dash and delete '_'.</xd:p>
		</xd:desc>
	</xd:doc>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Dates</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:date/text()">
		<xsl:call-template name="enDash"/>
	</xsl:template>
	<xsl:template name="enDash">
		<xsl:analyze-string select="." regex=".+([__]*)([-]*)">
			<xsl:matching-substring>
				<xsl:value-of select="translate(., '-__', '&#8211;')"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring> </xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Clean notes: delete zotero tags, notes without @type.</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:note[@type = 'tags' or @type = 'accessed' or not(@type)]"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Clean notes: delete zotero tags, notes without @type dot at the end of string</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:note[not(@type = 'url')]/text()">
		<xsl:variable name="maintext">
			<xsl:value-of select="normalize-space(.)"/>
		</xsl:variable>
		<xsl:value-of select="functx:substring-before-last($maintext, '.')"/>
	</xsl:template>
	<xsl:template match="tei:note[@type = 'thesisType']">
		<xsl:choose>
			<xsl:when test="not(string(.))"/>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@* | node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>disable output-escaping in titles but keep &amp;. Preserves tags like <hi rend="italic"/></xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:title/text()">
		<xsl:analyze-string select="." regex="(.*?)(&amp;)(.*?)">
			<xsl:matching-substring>
				<xsl:variable name="start">
					<xsl:value-of select="normalize-space(regex-group(1))"/>
				</xsl:variable>
				<xsl:variable name="end">
					<xsl:value-of select="normalize-space(regex-group(3))"/>
				</xsl:variable>
				<xsl:value-of select="$start" disable-output-escaping="yes"/>
				<xsl:text> &amp; </xsl:text>
				<xsl:value-of select="$end" disable-output-escaping="yes"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="." disable-output-escaping="yes"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>add initials to biblStruct in @n attribute.</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:biblStruct">
		<xsl:element name="biblStruct" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:variable name="refid" select="./@xml:id"/>
			<xsl:if test="key('INITIALS', $refid, $inputTree)/@n">
				<xsl:attribute name="n">
					<xsl:value-of select="key('INITIALS', $refid, $inputTree)/@n"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>from http://www.xsltfunctions.com/xsl/</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:function name="functx:substring-before-last" as="xs:string">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:param name="delim" as="xs:string"/>
		<xsl:sequence select="
				if (matches($arg, functx:escape-for-regex($delim))) then
					replace($arg, concat('^(.*)', functx:escape-for-regex($delim), '.*'), '$1')
				else
					''"/>
	</xsl:function>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>from http://www.xsltfunctions.com/xsl/</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:function name="functx:escape-for-regex" as="xs:string">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:sequence select="replace($arg, '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))', '\\$1')"/>
	</xsl:function>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>from http://www.xsltfunctions.com/xsl/</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:function name="functx:substring-after-last-match" as="xs:string" xmlns:functx="http://www.functx.com">
		<xsl:param name="arg" as="xs:string?"/>
		<xsl:param name="regex" as="xs:string"/>
		<xsl:sequence select="replace($arg, concat('^.*', $regex), '')"/>
	</xsl:function>
</xsl:stylesheet>
