﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xsl tei html">
  <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
    <xd:desc>
      <xd:p><xd:b>Created on:</xd:b> March 23, 2015</xd:p>
      <xd:p><xd:b>updated on:</xd:b> July 28, 2016 for the Pyu Project.</xd:p>
      <xd:p><xd:b>Author:</xd:b> Emmanuelle Morlock</xd:p>
      <xd:p>IGLouvre project's stylesheet: Transformation of bibliographic data for quality control.</xd:p>
      <xd:p><xd:b>Last modified on:</xd:b> July 30, 2016</xd:p>
    </xd:desc>
  </xd:doc>
  <!-- todo : nombre de volumes -->
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
  <!-- +++++++++++++++++++++ éléments non affichés -->
  <xsl:template match="idno"/>
  <xsl:template match="//tei:respStmt/tei:resp"/>
  <!-- +++++++++++++++++++++ Templates nommés -->
  <!-- +++++++++++++++++++++ ================ -->
  <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
    <desc>Link to the zotero entry</desc>
  </doc>
  <xsl:template match="tei:biblStruct/@corresp">
    <xsl:variable name="uri" select="."/>
    <a href="{$uri}" target="_blank">Zot.</a>
  </xsl:template>
  <doc xmlns="http://www.oxygenxml.com/ns/doc/xsl">
    <desc>We do not display notes that comes from the Zotero export.</desc>
  </doc>
  <xsl:template match="tei:note">
    <xsl:choose>
      <!-- for pyu -->
      <xsl:when test="@type = 'accessed' or @type = 'tags' or @type = 'tag'"/>
      <xsl:when test="text()">
        <span class="note" xmlns="http://www.w3.org/1999/xhtml">
          <xsl:apply-templates/>
        </span>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="makeCreator">
    <xsl:param name="creator"/>
    <xsl:param name="separator"/>
    <xsl:choose>
      <xsl:when test="local-name() = 'author'">
        <xsl:apply-templates/>
        <xsl:value-of select="$separator"/>
      </xsl:when>
      <xsl:when test="local-name() = 'editor' and @role = 'scientificEditor'">
        <xsl:apply-templates/>
        <xsl:text>&#160;(ed.)</xsl:text>
        <xsl:value-of select="$separator"/>
      </xsl:when>
      <xsl:when test="local-name() = 'editor' and @role = 'seriesEditor'">
        <xsl:apply-templates/>
        <xsl:text>&#160;(dir.)</xsl:text>
        <xsl:value-of select="$separator"/>
      </xsl:when>
      <xsl:when test="local-name() = 'editor'">
        <xsl:apply-templates/>
        <xsl:text>&#160;(ed.)</xsl:text>
        <xsl:value-of select="$separator"/>
      </xsl:when>
    </xsl:choose>
    <!-- 'and' or 'comma' separator -->
  </xsl:template>
  <!--  template match -->
  <xsl:template match="tei:name[descendant::tei:reg[@type = 'popular' or @type = 'simplified']]">
    <xsl:value-of select="descendant::tei:reg[@type = 'simplified']"/>
    <xsl:if test="descendant::tei:reg[@type = 'popular']">
      <xsl:text> [</xsl:text>
      <xsl:value-of select="descendant::tei:reg[@type = 'popular']"/>
      <xsl:text>]</xsl:text>
    </xsl:if>
  </xsl:template>
  <xsl:template match="tei:biblScope">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="."/>
  </xsl:template>
  <xsl:template match="tei:biblScope[@unit = 'page']">
    <xsl:choose>
      <xsl:when test="not(@from) and not(@to)">
        <xsl:value-of select="."/>
      </xsl:when>
      <xsl:when test="@from and @to">
        <xsl:value-of select="@from"/>
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
        <xsl:value-of select="@to"/>
      </xsl:when>
      <xsl:when test="@from and not(@to)">
        <xsl:value-of select="@from"/>
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
      </xsl:when>
      <xsl:when test="@to and not(@from)">
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
        <xsl:value-of select="@from"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:biblScope[@unit = 'volume']">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="."/>
  </xsl:template>
  <xsl:template name="date">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="tei:imprint/tei:date">
    <xsl:choose>
      <xsl:when test="@when">
        <xsl:value-of select="."/>
      </xsl:when>
      <xsl:when test="@from and @to">
        <xsl:value-of select="."/>
      </xsl:when>
      <xsl:when test="@from and not(@to)">
        <xsl:value-of select="."/>
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
      </xsl:when>
      <xsl:when test="@to and not(@from)">
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
        <xsl:value-of select="."/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:imprint/tei:date" mode="attribute">
    <xsl:choose>
      <xsl:when test="@when">
        <xsl:value-of select="@when"/>
      </xsl:when>
      <xsl:when test="@from and @to">
        <xsl:value-of select="@from"/>
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
        <xsl:value-of select="@to"/>
      </xsl:when>
      <xsl:when test="@from and not(@to)">
        <xsl:value-of select="@from"/>
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
      </xsl:when>
      <xsl:when test="@to and not(@from)">
        <xsl:text>&#8211;<!-- en dash --></xsl:text>
        <xsl:value-of select="@from"/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="tei:forename">
    <!-- pour tester 
        <xsl:value-of select="concat('|',.,'|')"></xsl:value-of>
        <xsl:text> </xsl:text>
        si le prénom commence par une voyelle -->
    <xsl:analyze-string select="." regex="(^[aeoiuyéèàùâêîôûäëïöüAEIOUYÉÈÀÙÂÊÎÔÛÄËÏÖÜ])(.[^\-]*)(\-?)(.*)">
      <xsl:matching-substring>
        <!-- <xsl:text>voyelle</xsl:text> -->
        <tei:forename>
          <xsl:value-of select="concat(upper-case(regex-group(1)), '.')"/>
          <xsl:choose>
            <xsl:when test="not(regex-group(3) = '')">
              <xsl:value-of select="regex-group(3)"/>
              <xsl:value-of select="concat(upper-case(substring(regex-group(4), 1, 1)), '. ')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text> </xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </tei:forename>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <!-- si digramme + r ou l  -->
        <xsl:analyze-string select="." regex="(^[^aeoiuyéèàùâêîôûäëïöüAEIOUYÉÈÀÙÂÊÎÔÛÄËÏÖÜ])([hrl])([rl])(.[^\-]*)(\-?)(.*)">
          <xsl:matching-substring>
            <!-- <xsl:text>consonne trigr</xsl:text> -->
            <tei:forename>
              <!-- <xsl:text>1</xsl:text> -->
              <xsl:value-of select="upper-case(regex-group(1))"/>
              <xsl:value-of select="regex-group(2)"/>
              <xsl:value-of select="regex-group(3)"/>
              <xsl:text>.</xsl:text>
              <xsl:choose>
                <xsl:when test="not(regex-group(5) = '')">
                  <xsl:value-of select="regex-group(3)"/>
                  <xsl:value-of select="concat(upper-case(substring(regex-group(6), 1, 1)), '. ')"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:text> </xsl:text>
                </xsl:otherwise>
              </xsl:choose>
            </tei:forename>
          </xsl:matching-substring>
          <xsl:non-matching-substring>
            <!-- si digramme   -->
            <xsl:analyze-string select="." regex="(^[^aeoiuyéèàùâêîôûäëïöüAEIOUYÉÈÀÙÂÊÎÔÛÄËÏÖÜ])([hrl])(.[^\-]*)(\-?)(.*)">
              <xsl:matching-substring>
                <!-- <xsl:text>consonne digr</xsl:text> -->
                <tei:forename>
                  <!-- <xsl:text>2</xsl:text> -->
                  <xsl:value-of select="upper-case(regex-group(1))"/>
                  <xsl:value-of select="regex-group(2)"/>
                  <xsl:text>.</xsl:text>
                  <xsl:choose>
                    <xsl:when test="not(regex-group(4) = '')">
                      <xsl:value-of select="regex-group(4)"/>
                      <xsl:value-of select="concat(upper-case(substring(regex-group(5), 1, 1)), '. ')"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:text> </xsl:text>
                    </xsl:otherwise>
                  </xsl:choose>
                </tei:forename>
              </xsl:matching-substring>
              <xsl:non-matching-substring>
                <tei:forename>
                  <!-- 
                    <xsl:value-of select="concat('|',.,'|')"></xsl:value-of>
                    <xsl:value-of select="concat(upper-case(substring(.,1,1)),'. ')"/>  
                  -->
                  <xsl:analyze-string select="." regex="(.[^\-]*)(\-?)(.*)">
                    <xsl:matching-substring>
                      <tei:forename>
                        <!-- <xsl:text>3</xsl:text> -->
                        <!--  <xsl:value-of select="concat('|1',regex-group(1),'|2',regex-group(2),'|3',regex-group(3),'|4',regex-group(4),'|')"></xsl:value-of>-->
                        <xsl:value-of select="upper-case(substring(regex-group(1), 1, 1))"/>
                        <xsl:if test="not(starts-with(regex-group(1), ' '))">
                          <!-- bizarre mais seule manière trouvée de ne pas avoir un point isolé -->
                          <xsl:text>.</xsl:text>
                        </xsl:if>
                        <xsl:choose>
                          <xsl:when test="not(regex-group(2) = '')">
                            <xsl:value-of select="regex-group(2)"/>
                            <xsl:value-of select="concat(upper-case(substring(regex-group(3), 1, 1)), '. ')"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text> </xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                      </tei:forename>
                    </xsl:matching-substring>
                    <xsl:non-matching-substring>
                      <!--  <xsl:text>4</xsl:text>-->
                      <!--  
                           <xsl:value-of select="concat('|',.,'|')"></xsl:value-of>-->
                      <!-- <xsl:value-of select="upper-case(substring(.,1,1))"/>  -->
                    </xsl:non-matching-substring>
                  </xsl:analyze-string>
                </tei:forename>
              </xsl:non-matching-substring>
            </xsl:analyze-string>
          </xsl:non-matching-substring>
        </xsl:analyze-string>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  <!-- ++++++++++++++++++++++  template match -->
  <!-- +++++++++++++++++++++ ================ -->
  <!--  
   
    -->
  <!-- traitement des <i> et </i> dans les champs zotero pour gérer les italiques -->
  <xsl:template match="*" mode="zoteroItalics">
    <xsl:element name="{name()}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="zoteroItalics"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="text()" mode="zoteroItalics">
    <xsl:analyze-string select="." regex="(&lt;i&gt;)(.*)(&lt;/i&gt;)">
      <xsl:matching-substring>
        <span class="title" xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="regex-group(2)"/>
        </span>
      </xsl:matching-substring>
      <xsl:non-matching-substring>
        <!-- #EM ajout 20150827 : prise en compte des <sup>-->
        <xsl:analyze-string select="." regex="(&lt;sup&gt;)(.*)(&lt;/sup&gt;)">
          <xsl:matching-substring>
            <sup xmlns="http://www.w3.org/1999/xhtml">
              <xsl:value-of select="regex-group(2)"/>
            </sup>
          </xsl:matching-substring>
          <xsl:non-matching-substring>
            <xsl:value-of select="."/>
          </xsl:non-matching-substring>
        </xsl:analyze-string>
      </xsl:non-matching-substring>
    </xsl:analyze-string>
  </xsl:template>
  <xsl:template match="*" mode="codeXML">
    <span class="balise" xmlns="http://www.w3.org/1999/xhtml">
      <br/>
      <xsl:text>&lt;</xsl:text>
      <xsl:value-of select="local-name()"/>
      <xsl:apply-templates select="@*" mode="codeXML"/>
      <xsl:text>&gt;</xsl:text>
    </span>
    <xsl:apply-templates mode="codeXML"/>
  </xsl:template>
  <xsl:template match="@*" mode="codeXML">
    <xsl:value-of select="concat(' ', local-name(), '=', '&quot;', ., '&quot;')"/>
  </xsl:template>
  <xsl:template match="text()" mode="codeXML">
    <xsl:value-of select="."/>
  </xsl:template>
  <xsl:template match="tei:title">
    <xsl:choose>
      <xsl:when test="not(@type) or @type = 'simplified'">
        <xsl:call-template name="makeTitleSpan">
          <xsl:with-param name="atts" select="concat('title', ' ', @level, ' ', @type)"/>
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:if test="parent::tei:*/tei:title[@type = 'translation' or @type = 'transcription']">
          <xsl:text> [</xsl:text>
        </xsl:if>
      </xsl:when>
      <xsl:when test="@type = 'transcription' and not(parent::tei:*/tei:title[@type = 'translation'])">
        <xsl:call-template name="makeTitleSpan">
          <xsl:with-param name="atts" select="concat('title', ' ', @level, ' ', @type)"/>
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text>] </xsl:text>
      </xsl:when>
      <xsl:when test="@type = 'transcription' and parent::tei:*/tei:title[@type = 'translation']">
        <xsl:call-template name="makeTitleSpan">
          <xsl:with-param name="atts" select="concat('title', ' ', @level, ' ', @type)"/>
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text> &#x2014; </xsl:text>
        <xsl:variable name="translation" select="parent::tei:*/tei:title[@type = 'translation']"/>
        <xsl:variable name="atts" select="concat('title', ' ', $translation/@level, ' ', $translation/@type)"/>
        <span class="{normalize-space($atts)}" xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="$translation"/>
        </span>
        <xsl:text>] </xsl:text>
      </xsl:when>
      <xsl:when test="@type = 'translation'">
        <xsl:if test="not(ancestor::tei:biblStruct[1]/tei:*/tei:title[@type = 'transcription']) or not(ancestor::tei:biblStruct[1]/tei:*/tei:title[@type = 'simplified'])">
          <xsl:text> [</xsl:text>
        </xsl:if>
        <xsl:call-template name="makeTitleSpan">
          <xsl:with-param name="atts" select="concat('title', ' ', @level, ' ', @type)"/>
          <xsl:with-param name="type" select="@type"/>
        </xsl:call-template>
        <xsl:text>] </xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="makeTitleSpan">
    <xsl:param name="atts"/>
    <xsl:param name="type"/>
    <span class="{normalize-space($atts)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>
  <!-- titre d'ensemble -->
  <xsl:template match="tei:monogr[2]/tei:title">
    <xsl:call-template name="makeTitleSpan">
      <xsl:with-param name="atts" select="concat('title ensemble', ' ', @level)"/>
      <xsl:with-param name="type" select="@type"/>
    </xsl:call-template>
  </xsl:template>
  <xsl:template match="tei:meeting">
    <span class="meeting" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>
  <!-- short title en vedette  -->
  <xsl:template match="tei:title[@type = 'short']" mode="vedette">
    <span class="vedette" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="."/>
    </span>
  </xsl:template>
  <!-- lien vers full text en ligne -->
  <xsl:template match="tei:idno[@type = 'url']" mode="proofreading">
    <a href="{.}" title="{.}" target="_blank" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:text>See fulltext</xsl:text>
      <xsl:if test="not(position() = last())">
        <br/>
      </xsl:if>
    </a>
  </xsl:template>
  <xsl:template match="tei:idno[@type = 'url']">
    <a href="{.}" title="'See fulltext'" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="."/>
    </a>
  </xsl:template>
</xsl:stylesheet>
