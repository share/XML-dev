﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:res="http://purl.org/vocab/resourcelist/schema#"
	xmlns:z="http://www.zotero.org/namespaces/export#" xmlns:ctag="http://commontag.org/ns#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:bibo="http://purl.org/ontology/bibo/" xmlns:address="http://schemas.talis.com/2005/address/schema#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
	exclude-result-prefixes="tei xs xsl" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> june 2021</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: IGLouvre</xd:p>
			<xd:p>Modification: june 2021</xd:p>
			<xd:p>Object: re-organize collectors file</xd:p>
			<xd:p>Status: TEST</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>			
			<xd:p>Process document node and insert processing instructions for TEI publisher</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="/">
		<xsl:processing-instruction name="teipublisher">template="biography.html" odd="entities.odd" view="div"</xsl:processing-instruction>
	<xsl:copy>
			<xsl:apply-templates  select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>			
			<xd:p>Add date and responsability for the change element in teiHeader/revisionDesc</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:revisionDesc">		
		<xsl:copy>
			<!-- copie de tous les enfants -->
			<xsl:copy-of select="./*"/>
			<xsl:element name="change" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:attribute name="who">
					<xsl:text>iglpart:EM</xsl:text>
				</xsl:attribute>
				<xsl:attribute name="when">
					<xsl:text>2021-06-04</xsl:text>
				</xsl:attribute>
				<xsl:text>post-traitement du fichier des collectionneurs</xsl:text>
			</xsl:element>	
		</xsl:copy>
	</xsl:template>
	<!--<xsl:variable name="listPerson">
		<xsl:copy-of select="//tei:text//tei:body//tei:listPerson"/>
	</xsl:variable>-->
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>l'élément de contenu principal</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:listPerson[ancestor::tei:teiHeader]">	
		
		<xsl:copy>
			<!-- copie de tous les enfants -->
			<xsl:copy-of select="./*"/>
		</xsl:copy>
	
	</xsl:template>
	
	<xsl:template match="tei:particDesc">
		<xsl:copy>
		<xsl:element name="listPerson" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:for-each select="ancestor::tei:TEI//tei:person">
				<xsl:sort select="@xml:id"/>
				<xsl:element name="person" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:apply-templates select="@*"/>
					<xsl:apply-templates select="tei:persName" mode="header"/>
					<xsl:apply-templates select="tei:birth"/>
					<xsl:apply-templates select="tei:death"/>
					<xsl:apply-templates select="tei:nationality"/>
					
					<xsl:apply-templates select="tei:occupation"/>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
			<!--<xsl:element name="listOrg" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:for-each select="ancestor::tei:TEI//tei:org">
					<xsl:sort select="@xml:id"/>
					<xsl:copy>
						<!-\- copie de tous les enfants -\->
						<xsl:copy-of select="./*"/>
					</xsl:copy>
				</xsl:for-each>
			</xsl:element>-->
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="tei:listPerson[ancestor::tei:text]">
		<xsl:element name="div" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="type">
				<xsl:text>biographies</xsl:text>
			</xsl:attribute>
			<xsl:element name="head" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:text>Notices biographiques</xsl:text>
			</xsl:element>
			<xsl:for-each select="tei:person">
				<xsl:sort select="@xml:id"></xsl:sort>
				<xsl:element name="div" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="xml:id">
						<xsl:value-of select="concat('bio-',@xml:id)"/>
					</xsl:attribute>
					<xsl:attribute name="corresp">
						<xsl:value-of select="concat('#',@xml:id)"/>
					</xsl:attribute>
					<xsl:attribute name="type">
						<xsl:text>bio</xsl:text>
					</xsl:attribute>
					<xsl:element name="head" xmlns="http://www.tei-c.org/ns/1.0">
						<xsl:apply-templates select="tei:persName" mode="text"/>
					</xsl:element>
					<xsl:apply-templates select="tei:note"/>
					<xsl:apply-templates select="tei:figure"/>
					<xsl:apply-templates select="tei:listBibl"/>
					<xsl:apply-templates select="tei:linkGroup"/>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="tei:note[@type='short-bio']">
		<xsl:element name="p" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="tei:persName" mode="header">
		<xsl:element name="persName" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="xml:lang">
				<xsl:text>fr</xsl:text>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="tei:persName" mode="text">
		<xsl:element name="persName" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="ref">
				<xsl:value-of select="concat('#',ancestor::tei:person/@xml:id)"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	
	
	
</xsl:stylesheet>
