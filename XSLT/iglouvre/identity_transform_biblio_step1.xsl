﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:res="http://purl.org/vocab/resourcelist/schema#"
	xmlns:z="http://www.zotero.org/namespaces/export#" xmlns:ctag="http://commontag.org/ns#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:bibo="http://purl.org/ontology/bibo/" xmlns:address="http://schemas.talis.com/2005/address/schema#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
	exclude-result-prefixes="tei xs xsl" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	<!--<xsl:param name="inputTree" select="doc('bibliography.xml')"/>-->

	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Oct 2020</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: IGLouvre</xd:p>
			<xd:p>Modification: 28 December 2020</xd:p>
			<xd:p>Modification: 05 June 2021</xd:p>
			<xd:p>Object: get Date from Zotero exported file.</xd:p>
			<xd:p>Object: no need to correct ns iglbibl anymore.</xd:p>
			<xd:p>Object: substitute iglBibl to iglbibl (namespace prefix)</xd:p>
			<xd:p>Object: change xml:id in the exported zotero group bibliography (with the TEI translator) using the tag in the iglbibl namespace</xd:p>
			<xd:p>Status: PROD</xd:p>
			<xd:p>Problèmes notés : le translator comporte des erreurs. Nb de volumes est mis dans edition : ne pas utiliser - attention ancienne version du translator</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xsl:variable name="currentDate">
		<xsl:value-of select="//tei:sourceDesc//tei:date[1]"/>
	</xsl:variable>
	<xsl:variable name="extent">
		<xsl:value-of select="extent"/>
	</xsl:variable>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Substitutes the id in the tag to the @xml:id (and copies the former @xml:id in a @n attribute.</xd:p>
			<xd:p>The id used in the corpus files is added to the zotero item as a tag. The substitution permits the use of the fn:id in the ODDs.</xd:p>
			<xd:p>XSLT inspired by Syd Bauman' code (https://wiki.tei-c.org/index.php/Id-to-xml-id.xslt).</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:note[@type = 'tag']">
		<!--<xsl:variable name="tag" select="substring-after(.[@type = 'tag'][contains(., 'iglBibl')][1], 'iglBibl:')"/>-->
		<xsl:variable name="tag" select="substring-after(.[@type = 'tag'][contains(., 'iglbibl')][1], 'iglbibl:')"/>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<!--<xsl:choose>
				<xsl:when test="$tag != ''">
					<xsl:text>iglbibl:</xsl:text>
					<xsl:value-of select="$tag"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="./* | ./text() | ./processing-instruction() | ./comment()"/>
				</xsl:otherwise>
			</xsl:choose>-->
			<xsl:apply-templates select="./* | ./text() | ./processing-instruction() | ./comment()"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="tei:biblStruct">
		<xsl:element name="biblStruct" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:variable name="tag" select="substring-after(.//tei:note[@type = 'tag'][contains(., 'iglbibl')][1], 'iglbibl:')"/>
			<xsl:variable name="xmlid" select="./@xml:id"/>
			<xsl:variable name="newIDattr">
				<xsl:text>xml:id</xsl:text>
			</xsl:variable>
			<xsl:if test="$tag != ''">
				<xsl:attribute name="{$newIDattr}">
					<!-- For the value of the new xml:id= or ident= attribute, use -->
					<!-- the value of the note starting with 'iglBibl'= -->
					<xsl:value-of select="$tag"/>
				</xsl:attribute>
				<!-- copy the forme @xml:id in a @n attribute -->
				<xsl:attribute name="n">
					<xsl:value-of select="$xmlid"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$tag != ''">
					<xsl:attribute name="{$newIDattr}">
						<!-- For the value of the new xml:id= or ident= attribute, use -->
						<!-- the value of the note starting with 'iglBibl'= -->
						<xsl:value-of select="$tag"/>
					</xsl:attribute>
					<!-- copy the forme @xml:id in a @n attribute -->
					<xsl:attribute name="n">
						<xsl:value-of select="$xmlid"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="{$newIDattr}">						
						<xsl:value-of select="normalize-unicode($xmlid)"/>
					</xsl:attribute>
					<xsl:attribute name="n">
						<xsl:value-of select="$xmlid"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<!-- Now copy over all other attributes (i.e., all but n= and xml:id=) -->
			<xsl:copy-of select="attribute::*[not(name() = 'n') and not(name() = 'xml:id')]"/>
			<!-- Now copy over all the content -->
			<xsl:apply-templates select="./* | ./text() | ./processing-instruction() | ./comment()"/>
		</xsl:element>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Add langUsage and change in teiHeader/profileDesc</xd:p>
			<xd:p>Add date and responsability for the change element in teiHeader/revisionDesc</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:teiHeader">		
		<xsl:copy>
			<!-- copie de tous les enfants -->
			<xsl:copy-of select="./*"/>
		
		<xsl:element name="profileDesc" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:element name="langUsage" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:element name="language" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="ident">
						<xsl:text>fr</xsl:text>
					</xsl:attribute>
					<xsl:text>French</xsl:text>
				</xsl:element>
			</xsl:element>
		</xsl:element>
		<xsl:element name="revisionDesc" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:element name="change" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:attribute name="when" select="$currentDate"/>
				<xsl:text>Post-traitement de l'export Zotero.</xsl:text>
			</xsl:element>				
		</xsl:element>
		</xsl:copy>
	</xsl:template>

	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Change tei:title in teiHeader</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:teiHeader//tei:title">
		<xsl:copy>
			<xsl:text>Bibliographie générale du projet IGLouvre</xsl:text>
		</xsl:copy>
	</xsl:template>

	<!--<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Add head in div </xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:body">
		<xsl:copy>
			<xsl:element name="div" xmlns="http://www.tei-c.org/ns/1.0">

				<xsl:element name="head" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:text>Bibliographie : études, éditions, etc.</xsl:text>
				</xsl:element>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:copy>
	</xsl:template>-->

	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Correct zotero error regarding extent (extent content should be put in edition).</xd:p>
		</xd:desc>
	</xd:doc>
	
	<xsl:template match="tei:monogr">
		<xsl:copy-of select="attribute::*"/>
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:apply-templates select="tei:author"/>
			<xsl:apply-templates select="tei:meeting"/>
			<xsl:apply-templates select="tei:title"/>
			<xsl:apply-templates select="tei:idno"/>
			<xsl:apply-templates select="tei:editor"/>

			<!-- include edition if tei:extent -->
			<xsl:if test="tei:extent">
				<xsl:element name="edition" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:value-of select="tei:extent"/>
				</xsl:element>
			</xsl:if>
			<!-- traiter la suite  sauf idno-->
			<xsl:apply-templates select="*[not(local-name() = 'title' or local-name() = 'author' or local-name() = 'meeting' or local-name() = 'editor' or local-name() = 'idno')]"/>
			<!--<!-\- traiter idno -\->
			-->
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Delete extent</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:extent"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Delete head</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:head"/>

</xsl:stylesheet>
