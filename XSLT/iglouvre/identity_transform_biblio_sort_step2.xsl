﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:res="http://purl.org/vocab/resourcelist/schema#"
	xmlns:z="http://www.zotero.org/namespaces/export#" xmlns:ctag="http://commontag.org/ns#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:bibo="http://purl.org/ontology/bibo/" xmlns:address="http://schemas.talis.com/2005/address/schema#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
	exclude-result-prefixes="tei xs xsl" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	<!--<xsl:param name="inputTree" select="doc('bibliography.xml')"/>-->

	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> may 2021</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: IGLouvre</xd:p>
			<xd:p>Modification: may 2021</xd:p>
			<xd:p>Modification: 06 June 2021</xd:p>
			<xd:p>Object: delete @who on change</xd:p>
			<xd:p>Object: sort biblio by type and create divs - after first post treatment of biblio</xd:p>
			<xd:p>Status: PROD</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xsl:variable name="dateExport">
		<xsl:value-of select="//tei:sourceDesc//tei:date[1]"/>
	</xsl:variable>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>			
			<xd:p>Process document node and insert processing instructions for TEI publisher</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="/">
		<xsl:processing-instruction name="teipublisher">template="bibliography.html" odd="bibliography.odd" view="single"</xsl:processing-instruction>
		<xsl:processing-instruction name="xml-model">href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:processing-instruction>
		<xsl:processing-instruction name="xml-model">href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"</xsl:processing-instruction>
		<xsl:copy>
			<xsl:apply-templates  select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>			
			<xd:p>Add date and responsability for the change element in teiHeader/revisionDesc</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:revisionDesc">		
		<xsl:copy>
			<!-- copie de tous les enfants -->
			<xsl:copy-of select="./*"/>
			<xsl:element name="change" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:attribute name="when" select="$dateExport"/>					
				<xsl:text>Post-traitement de l'export Zotero : réorganisation de l'arbre XML autour de divisions (div).</xsl:text>
			</xsl:element>	
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>l'élément de contenu principal</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:listBibl">		
		<xsl:for-each-group select="//tei:biblStruct" group-by="./@type">
			<xsl:sort select="@type"/>
			<xsl:variable name="type">
				<xsl:value-of select="current-grouping-key()"/>
			</xsl:variable>
			<!-- div with type as attribute-->
			<xsl:element name="div" xmlns="http://www.tei-c.org/ns/1.0">
				<xsl:attribute name="xml:id" select="$type"/>
				<!-- head -->
				<xsl:element name="head" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:value-of select="$type"/>
				</xsl:element>
				<!-- n for listBibl -->
				<xsl:element name="listBibl" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="n" select="$type"/>					
					<xsl:for-each select="current-group()">
						<!-- Critères de tri -->
						<xsl:sort select="@xml:id"/>
						<xsl:copy>
							<xsl:apply-templates  select="@*|node()"/>
						</xsl:copy>
					</xsl:for-each>
				</xsl:element>
			</xsl:element>
		</xsl:for-each-group>
	</xsl:template>
</xsl:stylesheet>
