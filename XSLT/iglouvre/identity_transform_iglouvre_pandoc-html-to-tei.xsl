﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:res="http://purl.org/vocab/resourcelist/schema#"
	xmlns:z="http://www.zotero.org/namespaces/export#" xmlns:ctag="http://commontag.org/ns#" xmlns:dcterms="http://purl.org/dc/terms/"
	xmlns:bibo="http://purl.org/ontology/bibo/" xmlns:address="http://schemas.talis.com/2005/address/schema#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
	xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="tei xs xsl html" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> May 2023</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: IGLouvre</xd:p>
			<xd:p>Modification: 2023-05-025</xd:p>
			<xd:p/>
			<xd:p>Status: TEST</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="html:style | html:meta | html:title | html:head"/>
	<xsl:template match="html:div[@class = 'csl-block'][1] | html:div[@class = 'csl-block'][2]"/>
	<xsl:template match="html:html">
		<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:tei="http://www.tei-c.org/ns/1.0" xml:id="bibliography" type="Bibliography">
			<teiHeader>
				<fileDesc>
					<titleStmt>
						<title>Bibliographie du corpus IGLouvre</title>
					</titleStmt>
					<publicationStmt>
						<p><date>2023-05-05</date></p>
					</publicationStmt>
					<sourceDesc>
						<p>Born digital - export from <ref target="https://www.zotero.org/groups/248770/ig_louvre-bibliographie">Zotero Group Library
							IGLouvre</ref></p>
					</sourceDesc>
				</fileDesc>
			</teiHeader>
			<xsl:apply-templates select="node()"/>
		</TEI>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>changes</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="html:body">
		<xsl:element name="standOff" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="html:div[@id = 'refs']">
		<xsl:element name="listBibl" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="html:div[@class = 'csl-entry']">
		<xsl:element name="bibl" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="type">
				<xsl:call-template name="type"/>
			</xsl:attribute>
			<xsl:attribute name="corresp">
				<xsl:value-of select="replace(@id, 'ref-', '')"/>
			</xsl:attribute>
			<xsl:attribute name="n">
				<xsl:call-template name="short-title"/>
			</xsl:attribute>
			<xsl:attribute name="xml:id">
				<xsl:call-template name="igl-identifier">
					<xsl:with-param name="att">
						<xsl:value-of select="normalize-space(html:div[@class = 'csl-block'][2]/string())"/>
					</xsl:with-param>
					<xsl:with-param name="current">
						<xsl:value-of select="current()"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- bibl/abbr -->
	<xsl:template match="html:div[@class = 'csl-block'][3]">
		<xsl:element name="abbr" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- bibl/abbr/name -->
	<xsl:template match="html:span[@class = 'smallcaps'][parent::html:div]">
		<xsl:element name="name" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- bibl/bibl -->
	<xsl:template match="html:div[@class = 'csl-block'][4]">
		<xsl:element name="bibl" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- em -->
	<xsl:template match="html:em" xmlns="http://www.tei-c.org/ns/1.0">
		<xsl:element name="title">
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- div/span -->
	<xsl:template match="html:span[not(@class = 'smallcaps')]">
		<xsl:choose>
			<xsl:when test=".[ancestor::html:div[@class='csl-entry'][contains(html:div[1],'chapter') or contains(html:div[1],'article-journal')]]">				
				<xsl:element name="title" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:attribute name="level" select="'a'"/>
					<xsl:apply-templates select="node()"/>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="seg" xmlns="http://www.tei-c.org/ns/1.0">
					<xsl:apply-templates select="node()"/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:element name="title" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="level" select="'a'"/>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	
	<!-- a -->
	<xsl:template match="html:a">
		<xsl:element name="ref" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="target">
				<xsl:value-of select="@href"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- sup  -->
	<xsl:template match="html:sup">
		<xsl:element name="hi" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="rend">
				<xsl:value-of select="'simple:superscript'"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- sup  -->
	<xsl:template match="html:sub">
		<xsl:element name="hi" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:attribute name="rend">
				<xsl:value-of select="'simple:subscript'"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>templates</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template name="type">
		<xsl:value-of select="html:div[@class = 'csl-block'][1]/string()"/>
	</xsl:template>
	<xsl:template name="short-title">
		<!-- replace \s by '-' in case -->
		<xsl:value-of select="normalize-space(html:div[@class = 'csl-block'][3])"/>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>example source data: Ma0831, Ma2414, Ma3119, iglbibl:agora-38, Ma0956</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template name="igl-identifier">
		<xsl:param name="att"/>
		<xsl:param name="current"/>
		<xsl:choose>
			<xsl:when test="contains($att, 'iglbibl:')">
				<xsl:value-of select="translate(replace($att, '(.*)([i|I]gl[b|B]ibl:)(.[^,]*)(.*)', '$3'), ' ', '')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'no-iglbibl' || generate-id($current)"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
<!-- 
	
<div id="ref-http://zotero.org/groups/248770/items/KLD8IVQ8"
	class="csl-entry" role="listitem">
	<div class="csl-block">book</div>
	<div class="csl-block">Ma0831, Ma2414, Ma3119, iglbibl:agora-38, Ma0956</div>
	<div class="csl-block"><span class="smallcaps">Lawton</span>
		2017</div>
	<div class="csl-block">Carol L. Lawton, <em>Votive reliefs</em>,
		Princeton, The American School of Classical Studies at Athens,
		The Athenian Agora: results of excavations conducted by the
		American School of Classical Studies at Athens 38, 2017, online,
		<a href="https://www.jstor.org/stable/e26379851"
			>https://www.jstor.org/stable/e26379851</a>.</div>
</div>

 -->
