﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:tei="http://www.tei-c.org/ns/1.0" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:res="http://purl.org/vocab/resourcelist/schema#"
	xmlns:z="http://www.zotero.org/namespaces/export#"
	xmlns:ctag="http://commontag.org/ns#"
	xmlns:dcterms="http://purl.org/dc/terms/"
	xmlns:bibo="http://purl.org/ontology/bibo/"
	xmlns:address="http://schemas.talis.com/2005/address/schema#"
	xmlns:foaf="http://xmlns.com/foaf/0.1/"
	exclude-result-prefixes="tei xs xsl" version="2.0">
	<xsl:output method="xml" indent="yes"/>
	<!--<xsl:param name="inputTree" select="doc('bibliography.xml')"/>-->
	<xsl:key name="TAGS" match="//biblStruct" use="//note[@type='tag']"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Oct 2020</xd:p>
			<xd:p><xd:b>Author:</xd:b> @emma_morlock (CNRS)</xd:p>
			<xd:p>Project: IGLouvre</xd:p>
			<xd:p>Modification: 20 October 2020</xd:p>
			<xd:p>Object: change xml:id in the exported zotero group bibliography (with the TEI translator) using the tag in the iglBibl namespace</xd:p>
			<xd:p>Status: TEST</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:output indent="yes" method="xml" encoding="UTF-8"/>
	<xsl:strip-space elements="*"/>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>Main template</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
		<xd:desc>
			<xd:p>add id to biblStruct in @n attribute.</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:template match="tei:biblStruct">
		<xsl:element name="biblStruct" xmlns="http://www.tei-c.org/ns/1.0">
			<xsl:variable name="tagid" select="note[@type='tag'][1]"/>
			<xsl:if test="key('TAGS', $tagid)">
				<xsl:attribute name="n">
					<xsl:value-of select="key('TAGS', $refid, $inputTree)/@n"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
